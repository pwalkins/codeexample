﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="ResetPwd.aspx.vb" 
  Inherits="ResetPwd" title="Reset Your Password" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

  <asp:Panel ID="pnlReset" runat="server">
    <table class="table_menu_indent" border="0">   
      <tr><td colspan="3">&nbsp;</td></tr>    
      <tr><td colspan="3"><span class="menu_headings">Reset Your Password</span></td></tr>
      <tr><td colspan="3">&nbsp;</td></tr>    
      <tr><td colspan="3">Enter your new password for your ProThera&reg; online account below.<br />
        Your new password will be effective immediately.</td></tr>
      <tr><td colspan="3">&nbsp;</td></tr>    
      <tr>
        <td><b>New Password:</b></td>
        <td align="right">
          <asp:TextBox ID="txtResetKey" runat="server" Visible="false"></asp:TextBox>
          <asp:TextBox ID="txtPwd" runat="server" TextMode="Password" ToolTip="enter new password"></asp:TextBox>
        </td>
        <td>
          <asp:RequiredFieldValidator ID="reqPwd" runat="server" ControlToValidate="txtPwd"
            ErrorMessage="must enter a new password" ValidationGroup="grpLogin">*</asp:RequiredFieldValidator>
          <asp:CustomValidator ID="cvPwd" runat="server" ErrorMessage="Password must be between 6 and 15 characters." 
            ControlToValidate="txtPwd" ValidationGroup="grpLogin">*</asp:CustomValidator>        
          <asp:Label ID="lblPwdMsg" runat="server" Text="Label"></asp:Label>
        </td>
      </tr>
      <tr>
        <td><b>Confirm New Password:</b></td>
        <td align="right">
          <asp:TextBox ID="txtPwdConfirm" runat="server" TextMode="Password" ToolTip="confirm new password"></asp:TextBox>
        </td>
        <td>
          <asp:RequiredFieldValidator ID="reqPwdConfirm" runat="server" ControlToValidate="txtPwdConfirm"
            ErrorMessage="Must enter a password." ValidationGroup="grpLogin">*</asp:RequiredFieldValidator>
          <asp:CompareValidator ID="CompValidPasswords" runat="server" ControlToValidate="txtPwd"
            ErrorMessage="Passwords do not match. Please try again." Type="String" Operator="Equal" ControlToCompare="txtPwdConfirm" ValidationGroup="grpLogin">*</asp:CompareValidator>
        </td>
      </tr>
      <tr><td colspan="3">&nbsp;</td></tr>
      <tr>
        <td>&nbsp;</td>
        <td align="right">
          <asp:Button ID="btnSubmit" runat="server" Text="Submit" ValidationGroup="grpLogin" />
        </td>
        <td>&nbsp;</td>
      </tr>
    </table>
    
    <br /><asp:ValidationSummary ID="ValSum" DisplayMode="List" runat="server" 
      HeaderText="" ShowSummary="true" ValidationGroup="grpLogin" />
  </asp:Panel>
  
  <asp:Panel ID="pnlResetSaved" runat="server">
    <table class="table_menu_indent" border="0">   
      <tr><td>&nbsp;</td></tr>    
      <tr><td><span class="menu_headings">Password Updated</span></td></tr>
      <tr><td>&nbsp;</td></tr>
      <tr><td>Congratulations, your password has successfully been reset. You may now sign in.</td></tr>    
    </table>
  </asp:Panel>

  <asp:Panel ID="pnlResetError" runat="server">
    <table class="table_menu_indent" border="0">   
      <tr><td>&nbsp;</td></tr>    
      <tr><td><span class="menu_headings">Reset Error</span></td></tr>
      <tr><td>&nbsp;</td></tr>
      <tr><td>There was an error reseting your password. Please try again or call Customer Service at 
        888-488-2488 Mon - Fri 7:30am - 5:00pm (PT).</td></tr>    
    </table>
  </asp:Panel>
  
  <asp:Panel ID="pnlResetKeyExpError" runat="server" Visible="false">
    <table class="table_menu_indent" border="0">   
      <tr><td>&nbsp;</td></tr>    
      <tr><td><span class="menu_headings">Reset Your Password</span></td></tr>
      <tr><td>&nbsp;</td></tr>
      <tr><td>Your password reset link has expired. For security purposes, you have 24 hours to reset
        your password before the link expires. Please 
        <asp:HyperLink ID="lnkForgotPassword" runat="server" NavigateUrl="~/ForgotPassword.aspx" >click here</asp:HyperLink> 
        to request a new reset password link.</td></tr>    
    </table>
  </asp:Panel>

  <asp:Panel ID="pnlResetKeyError" runat="server" Visible="false">
    <table class="table_menu_indent" border="0">   
      <tr><td>&nbsp;</td></tr>    
      <tr><td><span class="menu_headings">Reset Your Password</span></td></tr>
      <tr><td>&nbsp;</td></tr>
      <tr><td class="text_error">The reset key is missing. The link from your email must match exactly.
        Please try again or call Customer Service at 888-488-2488.</td></tr>    
    </table>
    <asp:Label ID="lblErrorMsg" runat="server" Text="Label" Visible="false"></asp:Label>
  </asp:Panel>
  <br /><br />
</asp:Content>
