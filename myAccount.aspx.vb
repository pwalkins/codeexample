﻿Imports System.Data
Imports System.Data.SqlClient

'              <%#PrintSpecialty(Eval("Specialty"), Eval("Degree"), Eval("LicenseNo"))%>

Partial Class myAccount
  Inherits System.Web.UI.Page

  'Private Const COUNTRY_USA As Integer = 2

  Public UserID As Long = 0
  Private ConnectionString As String
  Private isFromShoppingCart As Boolean
  Private addressID As Long = 0
  Private AccountTypeStr As String
  Private isProfessional As Boolean = False
  Private PWD_MIN As Integer
  Private PWD_MAX As Integer

  Public SHIPPING_ADDRESS_MAX As Integer 'max number of shipping addresses allowed

  Public Sub UserMsgBox(ByVal sMsg As String)
    'displays a java alert (popup)
    Dim sb As New StringBuilder
    Dim oFormObject As System.Web.UI.Control

    sMsg = sMsg.Replace("'", "\'")
    sMsg = sMsg.Replace(Chr(34), "\" & Chr(34))
    sMsg = sMsg.Replace(vbCrLf, "\n")
    sMsg = "<script language=javascript>alert(""" & sMsg & """)</script>"
    sb = New StringBuilder
    sb.Append(sMsg)

    For Each oFormObject In Me.Controls
      If TypeOf oFormObject Is HtmlForm Then
        Exit For
      End If
    Next

    oFormObject.Controls.AddAt(oFormObject.Controls.Count, New LiteralControl(sb.ToString()))
  End Sub

  Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
    Dim AccountNumber As String = ""
    Page.Title = ConfigurationManager.AppSettings("TITLE_TAG") & Page.Title

    'Prof / Patient
    AccountTypeStr = Session(System.Configuration.ConfigurationManager.AppSettings("SESSION_LOGIN"))
    If (AccountTypeStr = System.Configuration.ConfigurationManager.AppSettings("LOGIN_PROFESSIONAL")) Then
      isProfessional = True
    End If

    ConnectionString = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString
    PWD_MIN = System.Configuration.ConfigurationManager.AppSettings("PWD_MIN_LEN")
    PWD_MAX = System.Configuration.ConfigurationManager.AppSettings("PWD_MAX_LEN")
    SHIPPING_ADDRESS_MAX = System.Configuration.ConfigurationManager.AppSettings("SHIPPING_ADDRESS_MAX")

    UserID = Session(System.Configuration.ConfigurationManager.AppSettings("SESSION_USERID"))
    If Not (UserID > 0) Then
      'timed out so need to leave this screen
      Response.Redirect(System.Configuration.ConfigurationManager.AppSettings("MAINFRM_DEFAULT"))
    End If
    'Get MOM Account Number
    Dim clsCust As New classCustomer
    clsCust.UserID = UserID
    clsCust.getUser()
    AccountNumber = clsCust.MomAccountNo

    checkPreviousPage()

    'combo boxes
    Me.SqlDataSourceCountry.ConnectionString = ConnectionString
    Me.SqlDataSourceAddressType.ConnectionString = ConnectionString
    Me.SqlDataSourceStateCode.ConnectionString = ConnectionString

    Me.SqlDataSourceOrders.ConnectionString = ConnectionString
    Me.SqlDataSourceOrders.SelectParameters(0).DefaultValue = UserID

    'User Account Information (select)
    Me.SqlDataSourceUser.ConnectionString = ConnectionString
    Me.SqlDataSourceUser.SelectParameters(0).DefaultValue = UserID

    Me.SqlDataSourceBillUser.ConnectionString = ConnectionString
    Me.SqlDataSourceBillUser.SelectParameters(0).DefaultValue = UserID

    'email / password
    Me.SqlDataSourceEmail.ConnectionString = ConnectionString
    Me.SqlDataSourceEmail.SelectParameters(0).DefaultValue = UserID
    Me.SqlDataSourceEmail.UpdateParameters(1).DefaultValue = UserID

    Me.SqlDataSourcePassword.ConnectionString = ConnectionString
    Me.SqlDataSourcePassword.SelectParameters(0).DefaultValue = UserID
    Me.SqlDataSourcePassword.InsertParameters(1).DefaultValue = UserID

    'Shipping Address (select)
    Me.SqlDataSourceUserIDAddress.ConnectionString = ConnectionString
    Me.SqlDataSourceUserIDAddress.SelectParameters(0).DefaultValue = 0
    Me.SqlDataSourceUserIDAddress.SelectParameters(1).DefaultValue = UserID
    Me.SqlDataSourceUserIDAddress.SelectParameters(2).DefaultValue = True

    'Shipping Address  (edit, insert, & delete)
    Me.SqlDataSourceAddress.ConnectionString = ConnectionString

    Me.SqlDataSourceAddress.SelectParameters(0).DefaultValue = addressID  'set this when the addressID is known
    Me.SqlDataSourceAddress.SelectParameters(1).DefaultValue = 0
    Me.SqlDataSourceAddress.SelectParameters(2).DefaultValue = False  'only show 1 address record

    If (Page.IsPostBack) Then
      'page is being edited
      If (Me.FormViewAccntInfo.CurrentMode = FormViewMode.Edit) Then
        '* CONTACT INFO - need to invisible the other panels
        Me.pnlBillingInfo.Visible = False
        Me.pnlOrders.Visible = False
        Me.pnlSupportMaterials.Visible = False
        Me.FormViewEmailPassword.Visible = False
      End If

    Else
      ''see if there are any Support Materials
      'Dim _count As Integer = 0
      'Dim clsSupMaterials As New ClassDbConn
      '_count = clsSupMaterials.getValue("SELECT count(ProductLineID) FROM dbo.fn_getDocProdLines('" & AccountNumber & "')")
      'If (_count > 0) Then
      '  Me.pnlSupportMaterials.Visible = True
      'End If

      'Account Information (edit)
      If (Me.FormViewAddress.CurrentMode = FormViewMode.Insert) Then

        If (isFromShoppingCart) Then
          'not coming from the select FormView. so need to set this differently.
          Me.SqlDataSourceAddress.InsertParameters(0).DefaultValue = UserID
        Else
          'this is in the InsertItemTemplate
          Dim txtText As TextBox = Me.FormViewAddress.FindControl("txtUserID")
          txtText.Text = UserID
        End If
      End If
    End If

    If (Me.pnlAccountInfo.Visible = True) Then
      'On the main screen, so see if Support Materials needs to be displayed.
      Dim _count As Integer = 0
      Dim clsSupMaterials As New ClassDbConn
      _count = clsSupMaterials.getValue("SELECT count(ProductLineID) FROM dbo.fn_getDocProdLines('" & AccountNumber & "')")
      If (_count > 0) Then
        Me.hdnIsSupportMaterials.Value = "1"
      Else
        Me.hdnIsSupportMaterials.Value = "0"
      End If
    End If
    chkSupportMaterialsPanel()

    Me.txtMsg.Text = Session(System.Configuration.ConfigurationManager.AppSettings("SESSION_LOGIN"))
  End Sub

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    'Page.MaintainScrollPositionOnPostBack = True  'maintain position after postback

    Dim totalShippingAddress As Integer = 0

    'get total number of shipping addresses
    totalShippingAddress = Me.DataListShipping.Items.Count

    Dim tmpLbl As Label
    tmpLbl = Me.FormViewEditPwd.FindControl("lblPwdMsg")
    tmpLbl.Text = "(" & PWD_MIN.ToString & "-" & PWD_MAX.ToString & " characters)"

    'See if net terms or billing info needs to be displayed
    displayNetTerms_Billinfo()
  End Sub

  Sub chkSupportMaterialsPanel()
    If (hdnIsSupportMaterials.Value = "1") Then
      Me.pnlSupportMaterials.Visible = True
    Else
      Me.pnlSupportMaterials.Visible = False
    End If
  End Sub

  Sub txtInsertShipZipCode_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    'Need to populate the city and state if we can find it.
    Dim clsZipCode As New ClassDbConn
    Dim CountryLstBox As DropDownList
    Dim tmpTextBox As TextBox

    CountryLstBox = Me.FormViewAddress.FindControl("ddlInsertShipCountry")

    If (CountryLstBox.SelectedValue = ClassLibrary.COUNTRYID_US) Then
      tmpTextBox = Me.FormViewAddress.FindControl("txtInsertShipZipCode")
      If (clsZipCode.getCityState(tmpTextBox.Text)) Then
        'found a city and state so set them on the form since the zip changed
        tmpTextBox = Me.FormViewAddress.FindControl("txtInsertShipCity")
        tmpTextBox.Text = clsZipCode.City 'set the city

        tmpTextBox = Me.FormViewAddress.FindControl("txtInsertShipState")
        tmpTextBox.Text = clsZipCode.State 'set the state

        checkInsertShipStateListbox()
      End If
    End If

    tmpTextBox = Me.FormViewAddress.FindControl("txtInsertShipCity")
    tmpTextBox.Focus()
  End Sub

  Sub txtEditShipZipCode_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    'Need to populate the city and state if we can find it.
    Dim clsZipCode As New ClassDbConn
    Dim CountryLstBox As DropDownList
    Dim tmpTextBox As TextBox

    CountryLstBox = Me.FormViewAddress.FindControl("ddlEditShipCountry")

    If (CountryLstBox.SelectedValue = ClassLibrary.COUNTRYID_US) Then
      tmpTextBox = Me.FormViewAddress.FindControl("txtEditShipZipCode")
      If (clsZipCode.getCityState(tmpTextBox.Text)) Then
        'found a city and state so set them on the form since the zip changed
        tmpTextBox = Me.FormViewAddress.FindControl("txtEditShipCity")
        tmpTextBox.Text = clsZipCode.City 'set the city

        tmpTextBox = Me.FormViewAddress.FindControl("txtEditShipState")
        tmpTextBox.Text = clsZipCode.State 'set the state

        checkEditShipStateListbox()
      End If
    End If
  End Sub

  Sub txtProfZipCode_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '* PROFESSIONAL *
    'Need to populate the city and state if we can find it.
    Dim clsZipCode As New ClassDbConn
    Dim CountryLstBox As DropDownList
    Dim tmpTextBox As TextBox

    CountryLstBox = Me.FormViewAccntInfo.FindControl("ddlProfCountry")

    If (CountryLstBox.SelectedValue = ClassLibrary.COUNTRYID_US) Then
      tmpTextBox = Me.FormViewAccntInfo.FindControl("txtProfZipCode")
      If (clsZipCode.getCityState(tmpTextBox.Text)) Then
        'found a city and state so set them on the form since the zip changed
        tmpTextBox = Me.FormViewAccntInfo.FindControl("txtProfCity")
        tmpTextBox.Text = clsZipCode.City 'set the city

        tmpTextBox = Me.FormViewAccntInfo.FindControl("txtProfState")
        tmpTextBox.Text = clsZipCode.State 'set the state

        checkStateListbox()
      End If
    End If

    If (Page.IsValid) Then
      tmpTextBox = Me.FormViewAccntInfo.FindControl("txtProfCity")
      tmpTextBox.Focus()
    End If
  End Sub

  Sub txtBillProfZipCode_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    'Need to populate the city and state if we can find it.
    Dim clsZipCode As New ClassDbConn
    Dim CountryLstBox As DropDownList
    Dim tmpTextBox As TextBox

    CountryLstBox = Me.FormViewBillInfo.FindControl("ddlBillProfCountry")

    If (CountryLstBox.SelectedValue = ClassLibrary.COUNTRYID_US) Then
      tmpTextBox = Me.FormViewBillInfo.FindControl("txtBillProfZipCode")
      If (tmpTextBox.Text.Length > 1) Then
        If (clsZipCode.getCityState(tmpTextBox.Text)) Then
          'found a city and state so set them on the form since the zip changed
          tmpTextBox = Me.FormViewBillInfo.FindControl("txtBillProfCity")
          tmpTextBox.Text = clsZipCode.City 'set the city

          tmpTextBox = Me.FormViewBillInfo.FindControl("txtBillProfState")
          tmpTextBox.Text = clsZipCode.State 'set the state

          checkBillStateListbox()

          'Need to set the focus on the city
          tmpTextBox = Me.FormViewBillInfo.FindControl("txtBillProfCity")
          tmpTextBox.Focus()
        End If
      End If
    End If
  End Sub

  Protected Sub ddlProfState_TextChanged()
    'need to update the hidden textbox for the state ListBox
    Dim tmpTextBox As TextBox
    Dim StateLstBox As DropDownList

    StateLstBox = Me.FormViewAccntInfo.FindControl("ddlProfState")
    tmpTextBox = Me.FormViewAccntInfo.FindControl("txtProfState")
    tmpTextBox.Text = StateLstBox.SelectedValue

    tmpTextBox = Me.FormViewAccntInfo.FindControl("txtProfPhone")
    tmpTextBox.Focus()
  End Sub

  Protected Sub ddlBillProfState_TextChanged()
    'need to update the hidden textbox for the state ListBox
    Dim tmpTextBox As TextBox
    Dim StateLstBox As DropDownList

    StateLstBox = Me.FormViewBillInfo.FindControl("ddlBillProfState")
    tmpTextBox = Me.FormViewBillInfo.FindControl("txtBillProfState")

    tmpTextBox.Text = StateLstBox.SelectedValue
  End Sub

  Protected Sub ddlEditShipState_TextChanged()
    'need to update the hidden textbox for the state ListBox
    Dim tmpTextBox As TextBox
    Dim StateLstBox As DropDownList

    StateLstBox = Me.FormViewAddress.FindControl("ddlEditShipState")
    tmpTextBox = Me.FormViewAddress.FindControl("txtEditShipState")

    tmpTextBox.Text = StateLstBox.SelectedValue
  End Sub

  Protected Sub ddlInsertShipState_TextChanged()
    'need to update the hidden textbox for the state ListBox
    Dim tmpTextBox As TextBox
    Dim StateLstBox As DropDownList

    StateLstBox = Me.FormViewAddress.FindControl("ddlInsertShipState")
    tmpTextBox = Me.FormViewAddress.FindControl("txtInsertShipState")

    tmpTextBox.Text = StateLstBox.SelectedValue
  End Sub

  Protected Sub ddlProfCountry_TextChanged()
    '* PROFESSIONAL FORM *
    'International - turn off the extra field validations.
    'USA - turn all of the validation back on.

    Dim tmpRev As RegularExpressionValidator
    Dim tmpLabel As Label
    Dim tmpTextBox As TextBox
    Dim CountryLstBox As DropDownList

    'Can't do this for an edit template since it gets run when it's first filled on the screen.
    'set the dirty flag
    ''Master.setDirtyFlag("1")

    CountryLstBox = Me.FormViewAccntInfo.FindControl("ddlProfCountry")

    If (CountryLstBox.SelectedValue > ClassLibrary.COUNTRYID_US) Then
      'International
      'StateLstBox.Visible = False

      tmpRev = Me.FormViewAccntInfo.FindControl("revProfZipCode")
      tmpRev.Enabled = False

      tmpRev = Me.FormViewAccntInfo.FindControl("revProfPhone")
      tmpRev.Enabled = False

      tmpRev = Me.FormViewAccntInfo.FindControl("revProfFax")
      tmpRev.Enabled = False

      tmpRev = Me.FormViewAccntInfo.FindControl("revProfAltPhone")
      tmpRev.Enabled = False

      tmpRev = Me.FormViewAccntInfo.FindControl("revProfZipCode")
      tmpRev.Enabled = False

      tmpTextBox = Me.FormViewAccntInfo.FindControl("txtProfZipCode")
      tmpTextBox.AutoPostBack = False
      tmpTextBox.CausesValidation = False
      '  txtProfAddress1.Focus()

      tmpLabel = Me.FormViewAccntInfo.FindControl("lblProfBusPhEx")
      tmpLabel.Visible = False

      tmpLabel = Me.FormViewAccntInfo.FindControl("lblProfAltPhEx")
      tmpLabel.Visible = False

      tmpLabel = Me.FormViewAccntInfo.FindControl("lblProfBusPhFaxEx")
      tmpLabel.Visible = False

    Else
      'USA
      'StateLstBox.Visible = True

      tmpRev = Me.FormViewAccntInfo.FindControl("revProfZipCode")
      tmpRev.Enabled = True

      tmpRev = Me.FormViewAccntInfo.FindControl("revProfPhone")
      tmpRev.Enabled = True

      tmpRev = Me.FormViewAccntInfo.FindControl("revProfFax")
      tmpRev.Enabled = True

      tmpRev = Me.FormViewAccntInfo.FindControl("revProfAltPhone")
      tmpRev.Enabled = True

      tmpRev = Me.FormViewAccntInfo.FindControl("revProfZipCode")
      tmpRev.Enabled = True

      tmpTextBox = Me.FormViewAccntInfo.FindControl("txtProfZipCode")
      tmpTextBox.CausesValidation = True
      tmpTextBox.AutoPostBack = True

      tmpLabel = Me.FormViewAccntInfo.FindControl("lblProfBusPhEx")
      tmpLabel.Visible = True

      tmpLabel = Me.FormViewAccntInfo.FindControl("lblProfAltPhEx")
      tmpLabel.Visible = True

      tmpLabel = Me.FormViewAccntInfo.FindControl("lblProfBusPhFaxEx")
      tmpLabel.Visible = True
    End If

    checkStateListbox()

    tmpTextBox = Me.FormViewAccntInfo.FindControl("txtProfAddress1")
    tmpTextBox.Focus()
  End Sub

  Protected Sub txtProfPhone_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    'If the format for the US phone number came in plain, then add the formatting. ex. 7754253632 -> 775-425-3632
    Dim tmpDdl As DropDownList
    Dim tmpTextBox As TextBox

    Page.Validate()
    If (Page.IsValid) Then
      tmpDdl = Me.FormViewAccntInfo.FindControl("ddlProfCountry")
      If (tmpDdl.SelectedValue = ClassLibrary.COUNTRYID_US) Then
        'Need to reformat the phone number if it's just all numbers
        tmpTextBox = Me.FormViewAccntInfo.FindControl("txtProfPhone")
        Dim str As String = tmpTextBox.Text
        If (str.Length = 10) Then
          tmpTextBox.Text = str.Substring(0, 3) + "-" + str.Substring(3, 3) + "-" + str.Substring(6, 4)
        End If
      End If
    End If

    tmpTextBox = Me.FormViewAccntInfo.FindControl("txtProfExt")
    tmpTextBox.Focus()
  End Sub

  Protected Sub txtProfFax_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    'If the format for the US phone number came in plain, then add the formatting. ex. 7754253632 -> 775-425-3632
    Dim tmpDdl As DropDownList
    Dim tmpTextBox As TextBox

    tmpDdl = Me.FormViewAccntInfo.FindControl("ddlProfCountry")
    If (tmpDdl.SelectedValue = ClassLibrary.COUNTRYID_US) Then
      'Need to reformat the phone number if it's just all numbers
      tmpTextBox = Me.FormViewAccntInfo.FindControl("txtProfFax")
      Dim str As String = tmpTextBox.Text
      If (str.Length = 10) Then
        tmpTextBox.Text = str.Substring(0, 3) + "-" + str.Substring(3, 3) + "-" + str.Substring(6, 4)
      End If
    End If

    tmpTextBox = Me.FormViewAccntInfo.FindControl("txtProfAltPhone")
    tmpTextBox.Focus()
  End Sub

  Protected Sub txtProfAltPhone_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    'If the format for the US phone number came in plain, then add the formatting. ex. 7754253632 -> 775-425-3632
    Dim tmpDdl As DropDownList
    Dim tmpTextBox As TextBox

    tmpDdl = Me.FormViewAccntInfo.FindControl("ddlProfCountry")
    If (tmpDdl.SelectedValue = ClassLibrary.COUNTRYID_US) Then
      'Need to reformat the phone number if it's just all numbers
      tmpTextBox = Me.FormViewAccntInfo.FindControl("txtProfAltPhone")
      Dim str As String = tmpTextBox.Text
      If (str.Length = 10) Then
        tmpTextBox.Text = str.Substring(0, 3) + "-" + str.Substring(3, 3) + "-" + str.Substring(6, 4)
      End If
    End If
  End Sub

  Protected Sub ddlInsertShipCountry_TextChanged()
    'International - turn off the extra field validations.
    'USA - turn all of the validation back on.

    Dim tmpRev As RegularExpressionValidator
    Dim tmpLabel As Label
    Dim tmpTextBox As TextBox
    Dim CountryLstBox As DropDownList
    CountryLstBox = Me.FormViewAddress.FindControl("ddlInsertShipCountry")

    If (CountryLstBox.SelectedValue > ClassLibrary.COUNTRYID_US) Then
      'International
      tmpRev = Me.FormViewAddress.FindControl("revInsertShipZipCode")
      tmpRev.Enabled = False

      tmpRev = Me.FormViewAddress.FindControl("revInsertShipZipCode")
      tmpRev.Enabled = False

      tmpTextBox = Me.FormViewAddress.FindControl("txtInsertShipZipCode")
      tmpTextBox.AutoPostBack = False
      tmpTextBox.CausesValidation = False

    Else
      'USA
      tmpRev = Me.FormViewAddress.FindControl("revInsertShipZipCode")
      tmpRev.Enabled = True

      tmpRev = Me.FormViewAddress.FindControl("revInsertShipZipCode")
      tmpRev.Enabled = True

      tmpTextBox = Me.FormViewAddress.FindControl("txtInsertShipZipCode")
      tmpTextBox.AutoPostBack = True
      tmpTextBox.CausesValidation = True
    End If

    checkInsertShipStateListbox()

    'Go to the next textbox, Address line 1
    tmpTextBox = Me.FormViewAddress.FindControl("txtInsertShipAddress1")
    tmpTextBox.Focus()
  End Sub

  Protected Sub ddlEditShipCountry_TextChanged()
    'International - turn off the extra field validations.
    'USA - turn all of the validation back on.

    Dim tmpRev As RegularExpressionValidator
    'Dim tmpLabel As Label
    Dim tmpTextBox As TextBox
    Dim CountryLstBox As DropDownList
    CountryLstBox = Me.FormViewAddress.FindControl("ddlEditShipCountry")

    If (CountryLstBox.SelectedValue > ClassLibrary.COUNTRYID_US) Then
      'International
      tmpRev = Me.FormViewAddress.FindControl("revEditShipZipCode")
      tmpRev.Enabled = False

      tmpRev = Me.FormViewAddress.FindControl("revEditShipZipCode")
      tmpRev.Enabled = False

      tmpTextBox = Me.FormViewAddress.FindControl("txtEditShipZipCode")
      tmpTextBox.AutoPostBack = False
      tmpTextBox.CausesValidation = False

    Else
      'USA
      tmpRev = Me.FormViewAddress.FindControl("revEditShipZipCode")
      tmpRev.Enabled = True

      tmpRev = Me.FormViewAddress.FindControl("revEditShipZipCode")
      tmpRev.Enabled = True

      tmpTextBox = Me.FormViewAddress.FindControl("txtEditShipZipCode")
      tmpTextBox.AutoPostBack = True
      tmpTextBox.CausesValidation = True
    End If

    checkEditShipStateListbox()

    tmpTextBox = Me.FormViewAddress.FindControl("txtEditShipAddress1")
    tmpTextBox.Focus()
  End Sub

  Protected Sub ddlBillProfCountry_TextChanged()
    '* PROFESSIONAL BILLING FORM *
    'International - turn off the extra field validations.
    'USA - turn all of the validation back on.

    Dim tmpRev As RegularExpressionValidator
    'Dim tmpLabel As Label
    Dim tmpTextBox As TextBox
    Dim CountryLstBox As DropDownList
    CountryLstBox = Me.FormViewBillInfo.FindControl("ddlBillProfCountry")

    If (CountryLstBox.SelectedValue > ClassLibrary.COUNTRYID_US) Then
      'International
      tmpRev = Me.FormViewBillInfo.FindControl("revBillProfZipCode")
      tmpRev.Enabled = False

      tmpRev = Me.FormViewBillInfo.FindControl("revBillProfZipCode")
      tmpRev.Enabled = False

      tmpTextBox = Me.FormViewBillInfo.FindControl("txtBillProfZipCode")
      tmpTextBox.AutoPostBack = False
      tmpTextBox.CausesValidation = False

    Else
      'USA
      tmpRev = Me.FormViewBillInfo.FindControl("revBillProfZipCode")
      tmpRev.Enabled = True

      tmpRev = Me.FormViewBillInfo.FindControl("revBillProfZipCode")
      tmpRev.Enabled = True

      tmpTextBox = Me.FormViewBillInfo.FindControl("txtBillProfZipCode")
      tmpTextBox.AutoPostBack = True
      tmpTextBox.CausesValidation = True
    End If

    checkBillStateListbox()

    'If (Page.IsPostBack) Then
    'Set the focus to the next box, address line 1
    tmpTextBox = Me.FormViewBillInfo.FindControl("txtBillProfAddress1")
    tmpTextBox.Focus()
    'End If
  End Sub

  Private Sub checkPreviousPage()
    'See if the previous page was the Shopping cart page. If so, need to go back there once everything
    'is finished here.
    Dim previousPage As String
    'Dim addressID As Long
    Dim cartAddressPage As String, cartCCPage As String

    isFromShoppingCart = False
    'Need to remove the tilde in the front of the string.
    cartAddressPage = System.Configuration.ConfigurationManager.AppSettings("PAGE_CARTADDRESS").Remove(0, 1)
    cartCCPage = System.Configuration.ConfigurationManager.AppSettings("PAGE_CARTCC").Remove(0, 1)

    'Get the Previous Page
    If (Page.IsPostBack) Then
      previousPage = Session(System.Configuration.ConfigurationManager.AppSettings("SESSION_PREVIOUSPAGE"))
    Else
      'Post back is always going to be this page
      previousPage = HttpContext.Current.Request.ServerVariables("HTTP_REFERER")
      Session(System.Configuration.ConfigurationManager.AppSettings("SESSION_PREVIOUSPAGE")) = previousPage
    End If

    If (previousPage.IndexOf(cartAddressPage) > 0) Or (previousPage.IndexOf(cartCCPage) > 0) Then
      'need to see if we are coming from the Cart-Address or Cart CC page. User wants to edit an address
      addressID = Session(System.Configuration.ConfigurationManager.AppSettings("SESSION_ADDRESSID"))
      If (addressID > 0) Then
        '** SHIPPING ADDRESS - edit mode **
        isFromShoppingCart = True
        Me.pnlAccountInfo.Visible = False
        Me.pnlBillingInfo.Visible = False
        Me.pnlPassword.Visible = False
        Me.pnlSupportMaterials.Visible = False
        Me.pnlOrders.Visible = False
        Me.pnlShipping.Visible = True

        Me.FormViewAddress.Visible = True
        Me.pnlShipping.Visible = True

        FormViewAddress.ChangeMode(FormViewMode.Edit)
        Me.SqlDataSourceAddress.SelectParameters(0).DefaultValue = addressID

      ElseIf (addressID = -1) Then
        '** SHIPPING ADDRESS - insert mode **
        isFromShoppingCart = True

        Me.DataListShipping.Visible = False
        Me.FormViewAddress.Visible = True

        Me.pnlAccountInfo.Visible = False
        Me.pnlBillingInfo.Visible = False
        Me.pnlOrders.Visible = False
        Me.pnlPassword.Visible = False
        Me.pnlSupportMaterials.Visible = False

        FormViewAddress.ChangeMode(FormViewMode.Insert)
        Me.SqlDataSourceAddress.InsertParameters(0).DefaultValue = UserID

      ElseIf (addressID = -2) Then
        '* BILLING ADDRESS - Edit *
        isFromShoppingCart = True

        Me.pnlAccountInfo.Visible = False
        Me.pnlBillingInfo.Visible = True
        Me.pnlPassword.Visible = False
        Me.pnlSupportMaterials.Visible = False
        Me.pnlOrders.Visible = False
        Me.pnlShipping.Visible = False

        Me.FormViewBillInfo.ChangeMode(FormViewMode.Edit)
        Me.SqlDataSourceBillUser.SelectParameters(0).DefaultValue = UserID
      End If
    End If
  End Sub

  Function PrintChkBoxAns(ByVal chkAnswer As Boolean) As String
    If (chkAnswer = True) Then
      PrintChkBoxAns = "YES"
    Else
      PrintChkBoxAns = "NO"
    End If
  End Function

  Function PrintSpecialty(ByVal strSpecialty As String, ByVal strDegree As String, ByVal strLicenseNo As String) As String
    Dim strOut As String
    strOut = ""

    If (strSpecialty.Length > 1) Then
      strOut = strOut & "Specialty: " & strSpecialty & ", "
    End If

    If (strDegree.Length > 1) Then
      strOut = strOut & strDegree & " - "
    End If

    If (strLicenseNo.Length > 1) Then
      strOut = strOut & strLicenseNo & "  "
    End If

    If (strOut.Length > 1) Then
      'remove the last 2 chars
      strOut = strOut.Remove(strOut.Length - 2, 2) & "<BR>"
    End If

    PrintSpecialty = strOut
  End Function

  'Function PrintHonorific(ByVal in_str As String) As String
  '  Dim clsLib = New ClassLibrary
  '  PrintHonorific = clsLib.PrintHonorific(in_str)
  'End Function

  'Function PrintAddressPart(ByVal in_str1 As String, ByVal in_str2 As String) As String
  '  Dim clsLib = New ClassLibrary
  '  PrintAddressPart = clsLib.PrintAddressPart(in_str1, in_str2)
  'End Function

  'Function PrintYesNo(ByVal in_str As String) As String
  '  Dim clsLib = New ClassLibrary
  '  PrintYesNo = clsLib.PrintYesNo(in_str)
  'End Function

  Function PrintHonorific(ByVal strIn As String) As String
    Dim strOut As String = ""

    If (strIn.Length > 0) Then
      strOut = ", " & strIn
    End If
    PrintHonorific = strOut
  End Function

  Function PrintLine(ByVal strFront As String, ByVal strIn As String, ByVal strEnd As String) As String
    'only print the line if strIn has something in it.
    Dim strOut As String = ""

    If (strIn.Length > 0) Then
      strOut = strFront & strIn & strEnd
    End If
    PrintLine = strOut
  End Function

  Function PrintAddressPart(ByVal strAdd1 As String, ByVal strAdd2 As String) As String
    Dim strOut As String

    strOut = strAdd1
    If (strAdd2.Length > 1) Then
      strOut = strOut & "<BR>" & strAdd2
    End If

    PrintAddressPart = strOut
  End Function

  Protected Sub checkBillProfQuestions()
    Dim tmpRfv As RequiredFieldValidator

    Dim tmpLabel As Label
    Dim tmpTextBox As TextBox
    'Dim tmpHlink As HyperLink

    setBillStateDDL() 'setup the state DropDownList box the first time.
    setBillStateListbox()

    If (isProfessional) Then
      '* PROFESSIONAL *
      'Honorific
      tmpLabel = Me.FormViewBillInfo.FindControl("lblBillProfHonorific")
      tmpLabel.Visible = True
      tmpTextBox = Me.FormViewBillInfo.FindControl("txtBillProfHonorific")
      tmpTextBox.Visible = True
      'tmpHlink = Me.FormViewBillInfo.FindControl("hlinkBillHonorific")
      'tmpHlink.Visible = True
      tmpRfv = Me.FormViewBillInfo.FindControl("rfvBillProfHonorific")
      tmpRfv.Enabled = True
      tmpLabel = Me.FormViewBillInfo.FindControl("lblBillProfHonorific2")
      tmpLabel.Visible = True

      'Company
      tmpLabel = Me.FormViewBillInfo.FindControl("lblBillProfCompany")
      tmpLabel.Visible = True
      tmpTextBox = Me.FormViewBillInfo.FindControl("txtBillProfCompany")
      tmpTextBox.Visible = True

    Else
      '* PATIENT *
      'Honorific
      tmpLabel = Me.FormViewBillInfo.FindControl("lblBillProfHonorific")
      tmpLabel.Visible = False
      tmpTextBox = Me.FormViewBillInfo.FindControl("txtBillProfHonorific")
      tmpTextBox.Visible = False
      'tmpHlink = Me.FormViewBillInfo.FindControl("hlinkBillHonorific")
      'tmpHlink.Visible = False
      tmpRfv = Me.FormViewBillInfo.FindControl("rfvBillProfHonorific")
      tmpRfv.Enabled = False
      tmpLabel = Me.FormViewBillInfo.FindControl("lblBillProfHonorific2")
      tmpLabel.Visible = False

      'Company
      tmpLabel = Me.FormViewBillInfo.FindControl("lblBillProfCompany")
      tmpLabel.Visible = False
      tmpTextBox = Me.FormViewBillInfo.FindControl("txtBillProfCompany")
      tmpTextBox.Visible = False
    End If

    If (isFromShoppingCart) Then
      'want the cancel button to go back to the cart page
      Dim tmp_btn2 As Button = Me.FormViewBillInfo.FindControl("btnCancel")
      tmp_btn2.PostBackUrl = System.Configuration.ConfigurationManager.AppSettings("PAGE_CARTCC")
    End If
  End Sub

  Protected Sub checkInsertShipQuestions()
    Dim tmpRfv As RequiredFieldValidator

    Dim tmpLabel As Label
    Dim tmpTextBox As TextBox
    'Dim tmpHlink As HyperLink

    setInsertShipStateDDL() 'setup the state DropDownList box the first time.
    setInsertShipStateListbox()

    If (isProfessional) Then
      '* PROFESSIONAL *
      'Honorific
      tmpLabel = Me.FormViewAddress.FindControl("lblInsertShipHonorific")
      tmpLabel.Visible = True
      tmpTextBox = Me.FormViewAddress.FindControl("txtInsertShipHonorific")
      tmpTextBox.Visible = True
      'tmpHlink = Me.FormViewAddress.FindControl("hlinkInsertShipHonorific")
      'tmpHlink.Visible = True
      tmpRfv = Me.FormViewAddress.FindControl("rfvInsertShipHonorific")
      tmpRfv.Enabled = True
      tmpLabel = Me.FormViewAddress.FindControl("lblInsertShipHonorific2")
      tmpLabel.Visible = True

      'Company
      tmpLabel = Me.FormViewAddress.FindControl("lblInsertShipCompany")
      tmpLabel.Visible = True
      tmpTextBox = Me.FormViewAddress.FindControl("txtInsertShipCompany")
      tmpTextBox.Visible = True

    Else
      '* PATIENT *
      'Honorific
      tmpLabel = Me.FormViewAddress.FindControl("lblInsertShipHonorific")
      tmpLabel.Visible = False
      tmpTextBox = Me.FormViewAddress.FindControl("txtInsertShipHonorific")
      tmpTextBox.Visible = False
      'tmpHlink = Me.FormViewAddress.FindControl("hlinkInsertShipHonorific")
      'tmpHlink.Visible = False
      tmpRfv = Me.FormViewAddress.FindControl("rfvInsertShipHonorific")
      tmpRfv.Enabled = False
      tmpLabel = Me.FormViewAddress.FindControl("lblInsertShipHonorific2")
      tmpLabel.Visible = False

      'Company
      tmpLabel = Me.FormViewAddress.FindControl("lblInsertShipCompany")
      tmpLabel.Visible = False
      tmpTextBox = Me.FormViewAddress.FindControl("txtInsertShipCompany")
      tmpTextBox.Visible = False
    End If

    If (isFromShoppingCart) Then
      Dim tmp_btn As Button = Me.FormViewAddress.FindControl("lnkBtnShipInsCancel")
      tmp_btn.PostBackUrl = System.Configuration.ConfigurationManager.AppSettings("PAGE_CARTADDRESS")
    End If
  End Sub

  Protected Sub checkEditShipStateListbox()
    Dim CountryLstBox As DropDownList
    Dim StateLstBox As DropDownList
    Dim StateTextBox As TextBox

    CountryLstBox = Me.FormViewAddress.FindControl("ddlEditShipCountry")
    StateLstBox = Me.FormViewAddress.FindControl("ddlEditShipState")
    StateTextBox = Me.FormViewAddress.FindControl("txtEditShipState")
    If (CountryLstBox.SelectedValue = ClassLibrary.COUNTRYID_US) Then
      StateLstBox.Visible = True
    Else
      StateLstBox.Visible = False
    End If
    StateTextBox.Visible = Not (StateLstBox.Visible)

    If (CountryLstBox.SelectedValue = ClassLibrary.COUNTRYID_US) Then
      Try
        StateLstBox.SelectedValue = Trim(StateTextBox.Text)  'put the state in the ListBox
      Catch ex As Exception
        StateLstBox.SelectedIndex = 0
      End Try

      If (StateLstBox.SelectedIndex = 0) Then
        'bad data so null out the state text box so that the validation works on the text box
        StateTextBox.Text = ""
      End If
    End If
  End Sub

  Protected Sub setEditShipStateListbox()
    'For USA, need to park the state in a hidden text box. Then when the user picks the state
    'in the DropDownList box, then it needs to be put into the text box.
    Dim stateDDL As DropDownList
    Dim tmpTextBox As TextBox

    stateDDL = Me.FormViewAddress.FindControl("ddlEditShipState")
    tmpTextBox = Me.FormViewAddress.FindControl("txtEditShipState")

    If (stateDDL.Visible) Then
      stateDDL.SelectedValue = tmpTextBox.Text  'put the state in the ListBox
      If (stateDDL.SelectedIndex = 0) Then
        'bad data so null out the state text box so that the validation works on the text box
        tmpTextBox.Text = ""
      End If
    End If
  End Sub

  Protected Sub setEditShipStateDDL()
    'Setup State DropDownList box the first time. Initialize the State DDL.
    Dim countryDDL As DropDownList
    Dim stateDDL As DropDownList
    Dim stateTextBox As TextBox

    stateDDL = Me.FormViewAddress.FindControl("ddlEditShipState")
    stateTextBox = Me.FormViewAddress.FindControl("txtEditShipState")

    'Check for International
    countryDDL = Me.FormViewAddress.FindControl("ddlEditShipCountry")
    If (countryDDL.SelectedValue = ClassLibrary.COUNTRYID_US) Then
      'US - turn on the state list box, hide the state text box
      stateDDL.Visible = True
    Else
      stateDDL.Visible = False
    End If

    'State ListBox - need to load the selectedvalue by hand from the Table. Bind doesn't work on a string field.
    stateTextBox.Visible = Not (stateDDL.Visible)
  End Sub

  Protected Sub checkInsertShipStateListbox()
    Dim CountryLstBox As DropDownList
    Dim StateLstBox As DropDownList
    Dim StateTextBox As TextBox

    CountryLstBox = Me.FormViewAddress.FindControl("ddlInsertShipCountry")
    StateLstBox = Me.FormViewAddress.FindControl("ddlInsertShipState")
    StateTextBox = Me.FormViewAddress.FindControl("txtInsertShipState")

    If (CountryLstBox.SelectedValue = ClassLibrary.COUNTRYID_US) Then
      StateLstBox.Visible = True
    Else
      StateLstBox.Visible = False
    End If
    StateTextBox.Visible = Not (StateLstBox.Visible)

    If (CountryLstBox.SelectedValue = ClassLibrary.COUNTRYID_US) Then
      Try
        StateLstBox.SelectedValue = Trim(StateTextBox.Text)  'put the state in the ListBox
      Catch ex As Exception
        StateLstBox.SelectedIndex = 0
      End Try

      If (StateLstBox.SelectedIndex = 0) Then
        'bad data so null out the state text box so that the validation works on the text box
        StateTextBox.Text = ""
      End If
    End If
  End Sub

  Protected Sub setInsertShipStateListbox()
    'For USA, need to park the state in a hidden text box. Then when the user picks the state
    'in the DropDownList box, then it needs to be put into the text box.
    Dim stateDDL As DropDownList
    Dim tmpTextBox As TextBox

    stateDDL = Me.FormViewAddress.FindControl("ddlInsertShipState")
    tmpTextBox = Me.FormViewAddress.FindControl("txtInsertShipState")

    If (stateDDL.Visible) Then
      stateDDL.SelectedValue = tmpTextBox.Text  'put the state in the ListBox
      If (stateDDL.SelectedIndex = 0) Then
        'bad data so null out the state text box so that the validation works on the text box
        tmpTextBox.Text = ""
      End If
    End If
  End Sub

  Protected Sub setInsertShipStateDDL()
    'Setup State DropDownList box the first time. Initialize the State DDL.
    Dim countryDDL As DropDownList
    Dim stateDDL As DropDownList
    Dim stateTextBox As TextBox

    stateDDL = Me.FormViewAddress.FindControl("ddlInsertShipState")
    stateTextBox = Me.FormViewAddress.FindControl("txtInsertShipState")

    'Check for International
    countryDDL = Me.FormViewAddress.FindControl("ddlInsertShipCountry")
    If (countryDDL.SelectedValue = ClassLibrary.COUNTRYID_US) Then
      'US - turn on the state list box, hide the state text box
      stateDDL.Visible = True
    End If

    'State ListBox - need to load the selectedvalue by hand from the Table. Bind doesn't work on a string field.
    '&&& put back
    stateTextBox.Visible = Not (stateDDL.Visible)
  End Sub

  Protected Sub checkEditShipQuestions()
    Dim tmpRfv As RequiredFieldValidator

    Dim tmpLabel As Label
    Dim tmpTextBox As TextBox
    'Dim tmpHlink As HyperLink

    setEditShipStateDDL() 'setup the state DropDownList box the first time.
    setEditShipStateListbox()

    If (isProfessional) Then
      '* PROFESSIONAL *
      'Honorific
      tmpLabel = Me.FormViewAddress.FindControl("lblEditShipHonorific")
      tmpLabel.Visible = True
      tmpTextBox = Me.FormViewAddress.FindControl("txtEditShipHonorific")
      tmpTextBox.Visible = True
      'tmpHlink = Me.FormViewAddress.FindControl("hlinkEditShipHonorific")
      'tmpHlink.Visible = True
      tmpRfv = Me.FormViewAddress.FindControl("rfvEditShipHonorific")
      tmpRfv.Enabled = True
      tmpLabel = Me.FormViewAddress.FindControl("lblEditShipHonorific2")
      tmpLabel.Visible = True

      'Company
      tmpLabel = Me.FormViewAddress.FindControl("lblEditShipCompany")
      tmpLabel.Visible = True
      tmpTextBox = Me.FormViewAddress.FindControl("txtEditShipCompany")
      tmpTextBox.Visible = True

    Else
      '* PATIENT *
      'Honorific
      tmpLabel = Me.FormViewAddress.FindControl("lblEditShipHonorific")
      tmpLabel.Visible = False
      tmpTextBox = Me.FormViewAddress.FindControl("txtEditShipHonorific")
      tmpTextBox.Visible = False
      'tmpHlink = Me.FormViewAddress.FindControl("hlinkEditShipHonorific")
      'tmpHlink.Visible = False
      tmpRfv = Me.FormViewAddress.FindControl("rfvEditShipHonorific")
      tmpRfv.Enabled = False
      tmpLabel = Me.FormViewAddress.FindControl("lblEditShipHonorific2")
      tmpLabel.Visible = False

      'Company
      tmpLabel = Me.FormViewAddress.FindControl("lblEditShipCompany")
      tmpLabel.Visible = False
      tmpTextBox = Me.FormViewAddress.FindControl("txtEditShipCompany")
      tmpTextBox.Visible = False
    End If

    'came from the cart, so don't give them some buttons.
    If (isFromShoppingCart) Then
      'Need to get rid of a few buttons
      'set delete button to invisible. 
      'Dim tmp_btn As Button = Me.FormViewAddress.FindControl("lnkBtnShipDelete")
      'tmp_btn.Visible = False

      'want the cancel button to go back to the cart page
      Dim tmp_btn2 As Button = Me.FormViewAddress.FindControl("lnkBtnShipCancel")
      tmp_btn2.PostBackUrl = System.Configuration.ConfigurationManager.AppSettings("PAGE_CARTADDRESS")

      Me.pnlAccountInfo.Visible = False
      Me.pnlBillingInfo.Visible = False
      Me.pnlOrders.Visible = False
      Me.pnlPassword.Visible = False
      Me.pnlSupportMaterials.Visible = False

      Me.pnlShipping.Visible = True
      Me.FormViewAddress.Visible = True
      Me.DataListShipping.Visible = False

    End If
  End Sub

  Protected Sub checkStateListbox()
    Dim CountryLstBox As DropDownList
    Dim StateLstBox As DropDownList
    Dim StateTextBox As TextBox

    CountryLstBox = Me.FormViewAccntInfo.FindControl("ddlProfCountry")
    StateLstBox = Me.FormViewAccntInfo.FindControl("ddlProfState")
    StateTextBox = Me.FormViewAccntInfo.FindControl("txtProfState")
    If (CountryLstBox.SelectedValue = ClassLibrary.COUNTRYID_US) Then
      StateLstBox.Visible = True
    Else
      StateLstBox.Visible = False
    End If
    StateTextBox.Visible = Not (StateLstBox.Visible)

    If (CountryLstBox.SelectedValue = ClassLibrary.COUNTRYID_US) Then
      Try
        StateLstBox.SelectedValue = Trim(StateTextBox.Text)  'put the state in the ListBox
      Catch ex As Exception
        StateLstBox.SelectedIndex = 0
      End Try

      If (StateLstBox.SelectedIndex = 0) Then
        'bad data so null out the state text box so that the validation works on the text box
        StateTextBox.Text = ""
      End If
    End If
  End Sub

  Protected Sub setStateListbox()
    'For USA, need to park the state in a hidden text box. Then when the user picks the state
    'in the DropDownList box, then it needs to be put into the text box.
    Dim stateDDL As DropDownList
    Dim tmpTextBox As TextBox

    stateDDL = Me.FormViewAccntInfo.FindControl("ddlProfState")
    tmpTextBox = Me.FormViewAccntInfo.FindControl("txtProfState")

    If (stateDDL.Visible) Then
      stateDDL.SelectedValue = tmpTextBox.Text  'put the state in the ListBox
      If (stateDDL.SelectedIndex = 0) Then
        'bad data so null out the state text box so that the validation works on the text box
        tmpTextBox.Text = ""
      End If
    End If
  End Sub

  Protected Sub setStateDDL()
    'Setup State DropDownList box the first time. Initialize the State DDL.
    Dim countryDDL As DropDownList
    Dim stateDDL As DropDownList
    Dim stateTextBox As TextBox

    stateDDL = Me.FormViewAccntInfo.FindControl("ddlProfState")
    stateTextBox = Me.FormViewAccntInfo.FindControl("txtProfState")

    'Check for International
    countryDDL = Me.FormViewAccntInfo.FindControl("ddlProfCountry")
    If (countryDDL.SelectedValue = ClassLibrary.COUNTRYID_US) Then
      'US - turn on the state list box, hide the state text box
      stateDDL.Visible = True
    End If

    'State ListBox - need to load the selectedvalue by hand from the Table. Bind doesn't work on a string field.
    stateTextBox.Visible = Not (stateDDL.Visible)
  End Sub

  Protected Sub setBillStateDDL()
    'Setup State DropDownList box the first time. Initialize the State DDL.
    Dim countryDDL As DropDownList
    Dim stateDDL As DropDownList
    Dim stateTextBox As TextBox

    stateDDL = Me.FormViewBillInfo.FindControl("ddlBillProfState")
    stateTextBox = Me.FormViewBillInfo.FindControl("txtBillProfState")

    'Check for International
    countryDDL = Me.FormViewBillInfo.FindControl("ddlBillProfCountry")
    If (countryDDL.SelectedValue = ClassLibrary.COUNTRYID_US) Then
      'US - turn on the state list box, hide the state text box
      stateDDL.Visible = True
    End If

    'State ListBox - need to load the selectedvalue by hand from the Table. Bind doesn't work on a string field.
    stateTextBox.Visible = Not (stateDDL.Visible)
  End Sub

  Protected Sub setBillStateListbox()
    'For USA, need to park the state in a hidden text box. Then when the user picks the state
    'in the DropDownList box, then it needs to be put into the text box.
    Dim stateDDL As DropDownList
    Dim tmpTextBox As TextBox

    stateDDL = Me.FormViewBillInfo.FindControl("ddlBillProfState")
    tmpTextBox = Me.FormViewBillInfo.FindControl("txtBillProfState")

    If (stateDDL.Visible) Then
      stateDDL.SelectedValue = tmpTextBox.Text  'put the state in the ListBox
      If (stateDDL.SelectedIndex = 0) Then
        'bad data so null out the state text box so that the validation works on the text box
        tmpTextBox.Text = ""
      End If
    End If
  End Sub

  Protected Sub checkBillStateListbox()
    Dim CountryLstBox As DropDownList
    Dim StateLstBox As DropDownList
    Dim StateTextBox As TextBox

    CountryLstBox = Me.FormViewBillInfo.FindControl("ddlBillProfCountry")
    StateLstBox = Me.FormViewBillInfo.FindControl("ddlBillProfState")
    StateTextBox = Me.FormViewBillInfo.FindControl("txtBillProfState")
    If (CountryLstBox.SelectedValue = ClassLibrary.COUNTRYID_US) Then
      StateLstBox.Visible = True
    Else
      StateLstBox.Visible = False
    End If
    StateTextBox.Visible = Not (StateLstBox.Visible)

    If (CountryLstBox.SelectedValue = ClassLibrary.COUNTRYID_US) Then
      Try
        StateLstBox.SelectedValue = Trim(StateTextBox.Text)  'put the state in the ListBox
      Catch ex As Exception
        StateLstBox.SelectedIndex = 0
      End Try

      If (StateLstBox.SelectedIndex = 0) Then
        'bad data so null out the state text box so that the validation works on the text box
        StateTextBox.Text = ""
      End If
    End If
  End Sub

  Protected Sub checkProfQuestions()
    Dim tmpRfv As RequiredFieldValidator
    Dim tmpLabel As Label
    Dim tmpTextBox As TextBox
    'Dim tmpHlink As HyperLink

    setStateDDL() 'setup the state DropDownList box the first time.
    setStateListbox()

    '** Setup the Professional / Patient fields **
    If (isProfessional) Then
      '* PROFESSIONAL *
      'Authorization Code
      tmpLabel = Me.FormViewAccntInfo.FindControl("lblAuthCode")
      tmpLabel.Visible = False

      'Honorific
      tmpLabel = Me.FormViewAccntInfo.FindControl("lblProfHonorific")
      tmpLabel.Visible = True
      tmpTextBox = Me.FormViewAccntInfo.FindControl("txtProfHonorific")
      tmpTextBox.Visible = True
      'tmpHlink = Me.FormViewAccntInfo.FindControl("hlinkHonorific")
      'tmpHlink.Visible = True
      tmpRfv = Me.FormViewAccntInfo.FindControl("rfvProfHonorific")
      tmpRfv.Enabled = True
      tmpLabel = Me.FormViewAccntInfo.FindControl("lblProfHonorific2")
      tmpLabel.Visible = True

      'Specialty
      tmpLabel = Me.FormViewAccntInfo.FindControl("lblProfSpecialty")
      tmpLabel.Visible = True
      tmpTextBox = Me.FormViewAccntInfo.FindControl("txtProfSpecialty")
      tmpTextBox.Visible = True

      'Web Address
      tmpLabel = Me.FormViewAccntInfo.FindControl("lblProfURL")
      tmpLabel.Visible = True
      tmpTextBox = Me.FormViewAccntInfo.FindControl("txtProfURL")
      tmpTextBox.Visible = True

      'Business Address
      tmpLabel = Me.FormViewAccntInfo.FindControl("lblTitle2")
      tmpLabel.Text = "<br /><b>Business Address</b><br /><br />"

      'Company
      tmpLabel = Me.FormViewAccntInfo.FindControl("lblProfCompany")
      tmpLabel.Visible = True
      tmpTextBox = Me.FormViewAccntInfo.FindControl("txtProfCompany")
      tmpTextBox.Visible = True

      'Business Phone
      tmpLabel = Me.FormViewAccntInfo.FindControl("lblBusPh")
      tmpLabel.Text = "Business Phone:"

      'Fax
      tmpLabel = Me.FormViewAccntInfo.FindControl("lblBusPax")
      tmpLabel.Text = "Business Fax:"

    Else
      '* PATIENT *
      'Authorization Code
      tmpLabel = Me.FormViewAccntInfo.FindControl("lblAuthCode")
      tmpLabel.Visible = True

      'Honorific
      tmpLabel = Me.FormViewAccntInfo.FindControl("lblProfHonorific")
      tmpLabel.Visible = False
      tmpTextBox = Me.FormViewAccntInfo.FindControl("txtProfHonorific")
      tmpTextBox.Visible = False
      'tmpHlink = Me.FormViewAccntInfo.FindControl("hlinkHonorific")
      'tmpHlink.Visible = False
      tmpRfv = Me.FormViewAccntInfo.FindControl("rfvProfHonorific")
      tmpRfv.Enabled = False
      tmpLabel = Me.FormViewAccntInfo.FindControl("lblProfHonorific2")
      tmpLabel.Visible = False

      'Specialty
      tmpLabel = Me.FormViewAccntInfo.FindControl("lblProfSpecialty")
      tmpLabel.Visible = False
      tmpTextBox = Me.FormViewAccntInfo.FindControl("txtProfSpecialty")
      tmpTextBox.Visible = False

      'Web Address
      tmpLabel = Me.FormViewAccntInfo.FindControl("lblProfURL")
      tmpLabel.Visible = False
      tmpTextBox = Me.FormViewAccntInfo.FindControl("txtProfURL")
      tmpTextBox.Visible = False

      'Contact Info
      tmpLabel = Me.FormViewAccntInfo.FindControl("lblTitle2")
      tmpLabel.Text = "<br /><b>Contact Information</b><br /><br />"

      'Company
      tmpLabel = Me.FormViewAccntInfo.FindControl("lblProfCompany")
      tmpLabel.Visible = False
      tmpTextBox = Me.FormViewAccntInfo.FindControl("txtProfCompany")
      tmpTextBox.Visible = False

      'Phone
      tmpLabel = Me.FormViewAccntInfo.FindControl("lblBusPh")
      tmpLabel.Text = "Phone:"

      'Fax
      tmpLabel = Me.FormViewAccntInfo.FindControl("lblBusPax")
      tmpLabel.Text = "Fax:"
    End If
  End Sub

  '-----------------------------------------------------------------------------------------
  '** FormViewAccntInfo **
  Protected Sub FormViewAccntInfo_ItemUpdated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.FormViewUpdatedEventArgs) Handles FormViewAccntInfo.ItemUpdated
    If (isFromShoppingCart) Then
      'need to go back to the cc cart form

      'Don't erase the AddressID session otherwise the cart doesn't know what Address to display.
      'Session(System.Configuration.ConfigurationManager.AppSettings("SESSION_ADDRESSID")) = 0
      'reinit or loops back
      Session(System.Configuration.ConfigurationManager.AppSettings("SESSION_PREVIOUSPAGE")) = ""

      Response.Redirect(System.Configuration.ConfigurationManager.AppSettings("PAGE_CARTCC"))
    End If
  End Sub

  Protected Sub FormViewAccntInfo_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles FormViewAccntInfo.DataBound
    If Not (isFromShoppingCart) Then
      'Need to turn some things off/on after the data is bound to the form. ie. if non-US country, then
      'need to turn off some lables and validation checking for US formats.

      Select Case Me.FormViewAccntInfo.CurrentMode
        Case FormViewMode.Edit
          'Need to turn off some labels and validation if country is not US
          ddlProfCountry_TextChanged()

          checkProfQuestions()

          'Set the focus to the first question
          Dim txtBox As TextBox
          txtBox = Me.FormViewAccntInfo.FindControl("txtProfFirstName")
          txtBox.Focus()
      End Select
    End If
  End Sub

  Protected Sub FormViewAccntInfo_ModeChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.FormViewModeEventArgs) Handles FormViewAccntInfo.ModeChanging
    Select Case e.NewMode
      Case FormViewMode.ReadOnly
        Me.pnlShipping.Visible = True

        Me.pnlBillingInfo.Visible = True
        Me.pnlOrders.Visible = True
        'Me.pnlSupportMaterials.Visible = True    'This should be set to what it was before.
        Me.pnlPassword.Visible = True
        chkSupportMaterialsPanel()

      Case FormViewMode.Edit
        Me.pnlShipping.Visible = False

        Me.pnlBillingInfo.Visible = False
        Me.pnlOrders.Visible = False
        Me.pnlSupportMaterials.Visible = False
        Me.pnlPassword.Visible = False
    End Select
  End Sub

  '-----------------------------------------------------------------------------------------
  '** FormViewBillInfo **
  Protected Sub displayNetTerms_Billinfo()
    'Need to figure out if the Net Terms message needs to be displayed or the billing Information.
    Dim txtTemp As TextBox

    If (pnlBillingInfo.Visible) And (Me.FormViewAccntInfo.CurrentMode = FormViewMode.ReadOnly) Then
      txtTemp = Me.FormViewAccntInfo.FindControl("txtNetTerms")

      If (txtTemp.Text = "True") Then
        'Display Net Terms
        Me.tblBillingNetTerms.Visible = True
        Me.FormViewBillInfo.Visible = False

      Else
        'Display billing info
        Me.tblBillingNetTerms.Visible = False
        Me.FormViewBillInfo.Visible = True
      End If
    End If
  End Sub

  Protected Sub FormViewBillInfo_ItemUpdated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.FormViewUpdatedEventArgs) Handles FormViewBillInfo.ItemUpdated
    If (isFromShoppingCart) Then
      'need to go back to the cc cart form

      'Don't erase the AddressID session otherwise the cart doesn't know what Address to display.
      'Session(System.Configuration.ConfigurationManager.AppSettings("SESSION_ADDRESSID")) = 0
      'reinit or loops back
      Session(System.Configuration.ConfigurationManager.AppSettings("SESSION_PREVIOUSPAGE")) = ""
      Response.Redirect(System.Configuration.ConfigurationManager.AppSettings("PAGE_CARTCC"))
    End If
  End Sub

  Protected Sub FormViewBillInfo_ModeChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.FormViewModeEventArgs) Handles FormViewBillInfo.ModeChanging
    Select Case e.NewMode
      Case FormViewMode.ReadOnly
        Me.pnlShipping.Visible = True

        Me.pnlAccountInfo.Visible = True
        Me.pnlOrders.Visible = True
        'Me.pnlSupportMaterials.Visible = True
        Me.pnlPassword.Visible = True
        chkSupportMaterialsPanel()
        displayNetTerms_Billinfo()

      Case FormViewMode.Edit
        Me.pnlShipping.Visible = False

        Me.pnlAccountInfo.Visible = False
        Me.pnlOrders.Visible = False
        Me.pnlSupportMaterials.Visible = False
        Me.pnlPassword.Visible = False

        Me.tblBillingNetTerms.Visible = False
        Me.FormViewBillInfo.Visible = True    'editing this form
    End Select
  End Sub

  Protected Sub FormViewBillInfo_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles FormViewBillInfo.DataBound
    'Need to turn some things off/on after the data is bound to the form. ie. if non-US country, then
    'need to turn off some labels and validation checking for US formats.

    Select Case Me.FormViewBillInfo.CurrentMode
      Case FormViewMode.Edit
        'Need to turn off some labels and validation if country is not US
        ddlBillProfCountry_TextChanged()

        checkBillProfQuestions()

        'Switching to edit mode, so make sure the first textbox has focus.
        Dim tmpTxtBox As TextBox
        tmpTxtBox = Me.FormViewBillInfo.FindControl("txtBillProfFirstName")
        tmpTxtBox.Focus()
    End Select
  End Sub

  '-----------------------------------------------------------------------------------------
  '** FormViewAddress **
  Protected Sub FormViewAddress_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles FormViewAddress.DataBound
    If (FormViewAddress.CurrentMode = FormViewMode.Edit) Then
      ddlEditShipCountry_TextChanged()
      checkEditShipQuestions()

      'Switching to edit mode, so make sure the first textbox has focus.
      Dim tmpTxtBox As TextBox
      tmpTxtBox = Me.FormViewAddress.FindControl("txtEditShipFirstName")
      tmpTxtBox.Focus()

    ElseIf (FormViewAddress.CurrentMode = FormViewMode.Insert) Then
      checkInsertShipQuestions()
    End If
  End Sub

  Protected Sub FormViewAddress_ItemCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.FormViewCommandEventArgs) Handles FormViewAddress.ItemCommand
    'If update/insert pressed, then need to go back to the main My Accounts screen. So turn everything back on.
    Me.pnlAccountInfo.Visible = True
    Me.pnlBillingInfo.Visible = True
    Me.pnlOrders.Visible = True
    Me.pnlPassword.Visible = True
    Me.pnlShipping.Visible = True
    'Me.pnlSupportMaterials.Visible = True
    chkSupportMaterialsPanel()

    Me.FormViewAddress.Visible = False
    Me.DataListShipping.Visible = True
  End Sub

  Protected Sub FormViewAddress_ItemCreated(ByVal sender As Object, ByVal e As System.EventArgs) Handles FormViewAddress.ItemCreated
    'Need to default the UserID otherwise it crashes the INSERT command. 
    'UserID is set in the PageLoad using a session ID.
    If (Me.FormViewAddress.CurrentMode = FormViewMode.Insert) Then
      Dim txtTemp As TextBox
      txtTemp = Me.FormViewAddress.FindControl("txtUserID")
      txtTemp.Text = UserID
    End If
  End Sub

  'Protected Sub FormViewAddress_ItemDeleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.FormViewDeletedEventArgs) Handles FormViewAddress.ItemDeleted
  '  'address deleted so go back to the original templates showing, account info and shipping addresses
  '  Me.DataListShipping.Visible = True

  '  'refresh the query and the template
  '  Me.SqlDataSourceUserIDAddress.DataBind()
  '  Me.DataListShipping.DataBind()

  '  Me.FormViewAddress.Visible = False
  '  Me.FormViewAccntInfo.Visible = True
  'End Sub

  Protected Sub FormViewAddress_ItemInserted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.FormViewInsertedEventArgs) Handles FormViewAddress.ItemInserted
    'refresh the query and the template
    Me.SqlDataSourceUserIDAddress.DataBind()
    Me.DataListShipping.DataBind()

    If (isFromShoppingCart) Then
      'need to go back to the shopping cart page

      Session(System.Configuration.ConfigurationManager.AppSettings("SESSION_PREVIOUSPAGE")) = ""
      Response.Redirect(System.Configuration.ConfigurationManager.AppSettings("PAGE_CARTADDRESS"))
    End If
  End Sub

  Protected Sub FormViewAddress_ItemUpdated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.FormViewUpdatedEventArgs) Handles FormViewAddress.ItemUpdated
    'refresh the query and the template
    Me.SqlDataSourceUserIDAddress.DataBind()
    Me.DataListShipping.DataBind()

    'Me.txtMsg.Text = Me.txtMsg.Text & "- " & "ItemUpdated, isFromShoppingCart: " & isFromShoppingCart

    If (isFromShoppingCart) Then
      'need to go back to the shopping cart page

      'Don't erase the AddressID session otherwise the cart doesn't know what Address to display.
      'Session(System.Configuration.ConfigurationManager.AppSettings("SESSION_ADDRESSID")) = 0
      'reinit or loops back
      Session(System.Configuration.ConfigurationManager.AppSettings("SESSION_PREVIOUSPAGE")) = ""

      Response.Redirect(System.Configuration.ConfigurationManager.AppSettings("PAGE_CARTADDRESS"))
    End If
  End Sub

  '--------------------------------------------------------------------------------------------
  '** DataListShipping **
  Protected Sub DataListShipping_ItemCommnd(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles DataListShipping.ItemCommand

    If (e.CommandName = "AddNewAddress") Or (e.CommandName = "UpdateAddress") Then
      'turn all the other templates invisible, then turn on only the formview to insert a new address (FormViewAddress)
      Me.DataListShipping.Visible = False
      Me.FormViewAddress.Visible = True

      Me.pnlAccountInfo.Visible = False
      Me.pnlBillingInfo.Visible = False
      Me.pnlOrders.Visible = False
      Me.pnlPassword.Visible = False
      Me.pnlSupportMaterials.Visible = False
    End If

    If (e.CommandName = "AddNewAddress") Then
      If (CInt(Me.txtShipAddressCount.Text) = SHIPPING_ADDRESS_MAX) Then
        'Not allowed to create any more shipping addresses.
        'GoTo back to the main My Account page.
        UserMsgBox("Only a total of " & SHIPPING_ADDRESS_MAX.ToString & " shipping addresses allowed.\n Please delete one before adding another.")
        Me.DataListShipping.Visible = True
        Me.FormViewAddress.Visible = False
        Me.FormViewAccntInfo.Visible = True

        Me.pnlAccountInfo.Visible = True
        Me.pnlBillingInfo.Visible = True
        Me.pnlOrders.Visible = True
        Me.pnlPassword.Visible = True
        'Me.pnlSupportMaterials.Visible = True
        chkSupportMaterialsPanel()
      Else
        'insert a new addresss. passing in the UserID (don't have an AddressID yet)
        Me.FormViewAddress.ChangeMode(FormViewMode.Insert)  'change the formview to insert mode
        Me.SqlDataSourceAddress.InsertParameters(0).DefaultValue = UserID
      End If

    ElseIf (e.CommandName = "DeleteAddress") Then
      'don't delete if there is only 1 address left
      Me.SqlDataSourceUserIDAddress.DeleteParameters(0).DefaultValue = Convert.ToInt32(e.CommandArgument)
      Me.SqlDataSourceUserIDAddress.Delete()    'delete the address
      Me.SqlDataSourceUserIDAddress.DataBind()  'need to refresh the form

    Else 'If (e.CommandName = "UpdateAddress") Then
      'update the current address. passing in the AddressID. Need to have this default for Select (not update).
      Me.SqlDataSourceAddress.SelectParameters(0).DefaultValue = Convert.ToInt32(e.CommandArgument) 'update addressID parameter
    End If
  End Sub

  Protected Sub DataListShipping_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles DataListShipping.ItemDataBound
    'Need to save the total number of shipping addresses for the delete form (FormViewAddress).
    'Me.txtShipAddressCount.Text = Me.DataListShipping.Items.Count.ToString   'doesn't work here. Too early.

    If (e.Item.ItemType = ListItemType.Item) Or (e.Item.ItemType = ListItemType.AlternatingItem) Then
      'can't have the delete button if there is only 1 shipping address left
      Dim ShipToCount As Integer
      Dim tmpBtn As LinkButton

      ShipToCount = CInt(txtShipAddressCount.Text)
      tmpBtn = e.Item.FindControl("lnkBtnShipDelete")

      If (ShipToCount = 1) Then
        'need to disable the delete button
        tmpBtn.Visible = False
      Else
        tmpBtn.Visible = True
      End If
    End If
  End Sub

  '-----------------------------------------------------------------------------------------
  '** FormViewEmailPassword **
  Protected Sub FormViewEmailPassword_ItemCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.FormViewCommandEventArgs) Handles FormViewEmailPassword.ItemCommand
    If (e.CommandName = "EditEmailPwd") Then
      Me.pnlShipping.Visible = False
      Me.pnlAccountInfo.Visible = False
      Me.pnlBillingInfo.Visible = False
      Me.pnlOrders.Visible = False
      Me.pnlSupportMaterials.Visible = False

      Me.FormViewEmailPassword.Visible = False
      'Me.FormViewEditEmail.Visible = True
      Me.pnlEditEmail.Visible = True  '**
      Me.FormViewEditPwd.Visible = True
      Me.pnlPassword.Visible = True

      'make sure the form is changed to edit mode
      'Me.FormViewEditEmail.ChangeMode(FormViewMode.Edit)

      'Need to load up the current email address in password edit table
      Dim txtText As TextBox = Me.FormViewEmailPassword.FindControl("txtHiddenEmail")
      Me.txtEmail.Text = txtText.Text
    End If
  End Sub

  Protected Sub FormViewEmailPassword_ModeChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.FormViewModeEventArgs) Handles FormViewEmailPassword.ModeChanging
    'The SQL update doesn't work right without this for FormViewEditEmail.
    '** don't think this is being used any more. 1/25/2012.
    Select Case e.NewMode
      Case FormViewMode.ReadOnly
        Me.pnlShipping.Visible = True
        Me.pnlAccountInfo.Visible = True
        Me.pnlBillingInfo.Visible = True
        Me.pnlOrders.Visible = True
        'Me.pnlSupportMaterials.Visible = True
        chkSupportMaterialsPanel()

        Me.FormViewEmailPassword.Visible = True
        Me.pnlEditEmail.Visible = False  '**
        Me.FormViewEditPwd.Visible = False

      Case FormViewMode.Edit
        Me.pnlShipping.Visible = False
        Me.pnlAccountInfo.Visible = False
        Me.pnlBillingInfo.Visible = False
        Me.pnlOrders.Visible = False
        Me.pnlSupportMaterials.Visible = False

        Me.FormViewEmailPassword.Visible = False
        Me.pnlEditEmail.Visible = True  '**
        Me.FormViewEditPwd.Visible = True
    End Select
  End Sub

  '-----------------------------------------------------------------------------------------
  '** FormViewEditEmail **
  Protected Sub btnEmailCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEmailCancel.Click
    'Need to go back to the My Account main page.
    Me.pnlShipping.Visible = True
    Me.pnlAccountInfo.Visible = True
    Me.pnlBillingInfo.Visible = True
    Me.pnlOrders.Visible = True
    'Me.pnlSupportMaterials.Visible = True
    chkSupportMaterialsPanel()

    Me.pnlPassword.Visible = True
    Me.FormViewEmailPassword.Visible = True
    Me.FormViewEditPwd.Visible = False
    Me.pnlEditEmail.Visible = False
  End Sub

  Protected Sub btnEmailUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEmailUpdate.Click
    'Need to save the email if it's not already there
    Dim clsEmailUpdate As New classCustomer
    clsEmailUpdate.UserID = Me.UserID
    clsEmailUpdate.EmailAddress = Me.txtEmail.Text

    clsEmailUpdate.saveEmailAddress()

    'Check Return Code
    If (clsEmailUpdate.ReturnCode = 1) Then
      Me.lblEmailMsg.Visible = False

      'everything saved
      UserMsgBox("Email address updated")

      'Go back to the main My Account page
      Me.SqlDataSourceUser.DataBind() 'refresh the email on the main screen
      Me.FormViewEmailPassword.DataBind()

      Me.pnlShipping.Visible = True
      Me.pnlAccountInfo.Visible = True
      Me.pnlBillingInfo.Visible = True
      Me.pnlOrders.Visible = True
      'Me.pnlSupportMaterials.Visible = True
      chkSupportMaterialsPanel()

      Me.pnlPassword.Visible = True
      Me.FormViewEmailPassword.Visible = True
      pnlEditEmail.Visible = False
      Me.FormViewEditPwd.Visible = False

    Else
      'error
      Me.lblEmailMsg.Text = "Email already in use. Please try again."
      Me.lblEmailMsg.Visible = True
    End If
  End Sub

  '-----------------------------------------------------------------------------------------
  '** FormViewEditPwd **
  Protected Sub FormViewEditPwd_ItemCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.FormViewCommandEventArgs) Handles FormViewEditPwd.ItemCommand
    'cancel or updated pressed, need to get back to the main screens
    If (Page.IsValid) Then
      Me.pnlShipping.Visible = True
      Me.pnlAccountInfo.Visible = True
      Me.pnlBillingInfo.Visible = True
      Me.pnlOrders.Visible = True
      'Me.pnlSupportMaterials.Visible = True
      chkSupportMaterialsPanel()

      Me.pnlPassword.Visible = True
      Me.FormViewEmailPassword.Visible = True
      pnlEditEmail.Visible = False
      Me.FormViewEditPwd.Visible = False
    End If
  End Sub

  Protected Sub cvPassword_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) 'Handles cvPatPassword.ServerValidate
    If (PWD_MIN <= args.Value.Length) And (args.Value.Length <= PWD_MAX) Then
      args.IsValid = True
    Else
      args.IsValid = False
    End If
  End Sub

  'Protected Sub cvProfState_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs)
  '  'see if state listobx has a choice in it
  '  Dim tmpDDL As DropDownList
  '  tmpDDL = Me.FormViewAccntInfo.FindControl("ddlProfState")

  '  If (tmpDDL.SelectedValue <> "") Then
  '    args.IsValid = True
  '  Else
  '    args.IsValid = False
  '  End If
  'End Sub

  '---------------------------------------------------------------------------------------------
  '** SqlDataSource **

  Protected Sub SqlDataSourceAddress_Selected(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceStatusEventArgs) Handles SqlDataSourceAddress.Selected
    If (Not e.Exception Is Nothing) Then
      If (TypeOf e.Exception Is System.Data.SqlClient.SqlException) Then
        Me.txtMsg.Visible = True
        Me.txtMsg.Text = "Problem with SQL. Msg: " & e.Exception.Message
        e.ExceptionHandled = True
      End If
    End If
  End Sub

  Protected Sub SqlDataSourceEmail_Updated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceStatusEventArgs) Handles SqlDataSourceEmail.Updated
    If (e.Exception IsNot Nothing) Then
      'An exception has occured executing the SQL command and needs to be handled.

      'tell asp.net that the error has been handled so it can continue to executing the application code
      e.ExceptionHandled = True

      Me.lblEmailMsg.Text = "Email already in use. Please try again."
      Me.lblEmailMsg.Visible = True

    Else
      Me.lblEmailMsg.Visible = False

      Dim tmpInt As Integer
      tmpInt = e.Command.Parameters("@ReturnCode").Value

      'everything ok so display a message
      UserMsgBox("Email address updated")

      'need to update the main my accounts screen
      'refresh the query and the template
      Me.SqlDataSourceUser.DataBind()
      Me.FormViewEmailPassword.DataBind()
    End If
  End Sub

  Protected Sub SqlDataSourcePassword_Inserted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceStatusEventArgs) Handles SqlDataSourcePassword.Inserted
    If (e.Exception IsNot Nothing) Then
      e.ExceptionHandled = True

      Me.txtMsg.Text = "Error saving Password."
      Me.txtMsg.Visible = True

    Else
      'everything ok so display a message
      UserMsgBox("Password updated")
    End If
  End Sub

  Protected Sub SqlDataSourceUser_Updated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceStatusEventArgs) Handles SqlDataSourceUser.Updated
    If (e.Exception IsNot Nothing) Then
      e.ExceptionHandled = True   'need to reset the error
      Me.txtMsg.Text = "Error updating My Account."
      Me.txtMsg.Visible = True
    Else
      'everything ok so display a message
      UserMsgBox("My Account contact information updated")

      'Need to update the "Welcome" user info on the top right hand margin of the form (name and honorific).
      Dim clsCust As New classCustomer
      Dim ok As Boolean
      ok = clsCust.setUserInfo(UserID)

      'Need to refresh that part of the master page
      Master.setupCustName()
    End If
  End Sub

  Protected Sub SqlDataSourceBillUser_Updated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceStatusEventArgs) Handles SqlDataSourceBillUser.Updated
    If (e.Exception IsNot Nothing) Then
      e.ExceptionHandled = True   'need to reset the error
      Me.txtMsg.Text = "Error updating billing information."
      Me.txtMsg.Visible = True
    Else
      'everything ok so display a message
      UserMsgBox("My Account billing information updated")
    End If
  End Sub

  Protected Sub SqlDataSourceAddress_Updated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceStatusEventArgs) Handles SqlDataSourceAddress.Updated
    If (e.Exception IsNot Nothing) Then
      e.ExceptionHandled = True   'need to reset the error
      Me.txtMsg.Text = "Error updating shipping information."
      Me.txtMsg.Visible = True
    Else
      'everything ok so display a message
      UserMsgBox("My Account shipping information updated")
    End If
  End Sub

  Protected Sub SqlDataSourceAddress_Inserted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceStatusEventArgs) Handles SqlDataSourceAddress.Inserted
    If (e.Exception IsNot Nothing) Then
      e.ExceptionHandled = True   'need to reset the error
      Me.txtMsg.Text = "Error inserting shipping information."
      Me.txtMsg.Visible = True
    Else
      'everything ok so display a message
      UserMsgBox("My Account shipping information inserted")
      Me.SqlDataSourceAddress.DataBind()  'may need to rebind the data cause it was getting an error after the insert (when clicking on edit - address 1)
    End If
  End Sub

  Protected Sub SqlDataSourceAddress_Inserting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceCommandEventArgs) Handles SqlDataSourceAddress.Inserting
    Dim tmp As String

    'tmp = Me.SqlDataSourceAddress.InsertParameters
    tmp = e.Command.Parameters(8).Value

  End Sub

  'Protected Sub SqlDataSourceAddress_Deleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceStatusEventArgs) Handles SqlDataSourceAddress.Deleted
  '  If (e.Exception IsNot Nothing) Then
  '    e.ExceptionHandled = True   'need to reset the error
  '    Me.txtMsg.Text = "Error deleting shipping information."
  '    Me.txtMsg.Visible = True
  '  Else
  '    'everything ok so display a message
  '    UserMsgBox("My Account shipping information deleted")
  '  End If
  'End Sub

  Protected Sub SqlDataSourceUserIDAddress_Deleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceStatusEventArgs) Handles SqlDataSourceUserIDAddress.Deleted
    If (e.Exception IsNot Nothing) Then
      e.ExceptionHandled = True   'need to reset the error
      Me.txtMsg.Text = "Error deleting shipping information."
      Me.txtMsg.Visible = True
    Else
      'everything ok so display a message
      UserMsgBox("My Account shipping information deleted")
    End If

  End Sub

  Protected Sub SqlDataSourceUserIDAddress_Selected(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceStatusEventArgs) Handles SqlDataSourceUserIDAddress.Selected
    'Get the number of address records
    Me.txtShipAddressCount.Text = e.AffectedRows
  End Sub
End Class
