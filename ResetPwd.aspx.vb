﻿Partial Class ResetPwd
  Inherits System.Web.UI.Page

  '11/7/2011 Don't need the username/email address at all. Reset key is enough.

  '  <tr>
  '  <td>Email</td>
  '  <td><asp:TextBox ID="txtUsername" runat="server" ToolTip="enter username"></asp:TextBox>
  '  <asp:RequiredFieldValidator ID="reqUsername" runat="server" ControlToValidate="txtUsername"
  '  ErrorMessage="must enter a username">*</asp:RequiredFieldValidator></td>
  '</tr>

  Dim PWD_MIN As Integer
  Dim PWD_MAX As Integer

  Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
    Page.Title = ConfigurationManager.AppSettings("TITLE_TAG") & Page.Title
  End Sub

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    'First coming in, need to see if the reset key is there and see if the reset key is still valid.
    PWD_MIN = System.Configuration.ConfigurationManager.AppSettings("PWD_MIN_LEN")
    PWD_MAX = System.Configuration.ConfigurationManager.AppSettings("PWD_MAX_LEN")
    Me.lblPwdMsg.Text = "(" & PWD_MIN.ToString & "-" & PWD_MAX.ToString & " characters)"

    If (Page.IsPostBack) Then
    Else
      '* First time through *

      Me.txtResetKey.Text = Request.QueryString("ID")

      'Check for expired link.
      Dim isKeyValid = False
      Dim clsCust As New classCustomer
      isKeyValid = clsCust.chkPasswordResetKey(Me.txtResetKey.Text)

      If (Me.txtResetKey.Text.Length < 10) Then
        'Check for missing Reset Key.
        'ERROR. Reset key is missing.
        Me.pnlResetKeyError.Visible = True
        Me.pnlResetKeyExpError.Visible = False
        Me.pnlReset.Visible = False
        Me.pnlResetSaved.Visible = False
        Me.pnlResetError.Visible = False

      ElseIf Not (isKeyValid) Then
        'ERROR. Reset key is expired.
        Me.pnlResetKeyExpError.Visible = True
        Me.pnlReset.Visible = False
        Me.pnlResetSaved.Visible = False
        Me.pnlResetError.Visible = False
        'Me.lblErrorMsg.Text = clsCust.ErrorMsg  '&&&

      Else
        'Everything is ok so continue.
        Me.pnlReset.Visible = True
        Me.pnlResetKeyExpError.Visible = False
        Me.pnlResetKeyError.Visible = False
        Me.pnlResetSaved.Visible = False
        Me.pnlResetError.Visible = False

        Me.txtPwd.Focus()
      End If
    End If
  End Sub

  Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
    If (Page.IsValid) Then
      'save the records
      Dim clsCustomer As New classCustomer
      clsCustomer.Pwd = Me.txtPwd.Text

      clsCustomer.pwdResetKey = Me.txtResetKey.Text
      clsCustomer.Pwd = Me.txtPwd.Text
      clsCustomer.saveNewPassword()

      'get the number of records updated (should only be 1)
      Dim numRecordsSaved As Integer = clsCustomer.ReturnCode

      If (numRecordsSaved = 1) Then
        'everything ok. display ok message
        Me.pnlResetSaved.Visible = True
        Me.pnlReset.Visible = False
        Me.pnlResetError.Visible = False

      ElseIf (numRecordsSaved = 0) Then
        'display error. Error saving data.
        Me.pnlResetSaved.Visible = False
        Me.pnlReset.Visible = False
        Me.pnlResetError.Visible = True
      End If
    End If
  End Sub

    Protected Sub cvPwd_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles cvPwd.ServerValidate
        Dim pwd_len As Integer
        pwd_len = args.Value.Length

        If (pwd_len >= PWD_MIN) And (pwd_len <= PWD_MAX) Then
            'all is good
            args.IsValid = True
        Else
            'invalid password length
            args.IsValid = False
        End If
    End Sub
End Class
