﻿<%@ Page Language="VB" Debug="true" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" 
  CodeFile="myAccount.aspx.vb" Inherits="myAccount" title="Account Options" %>
<%@ MasterType VirtualPath="~/MasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script type="text/javascript">
/* function showHonorifics() {
  window.open('/forms/honorifics.htm','honorifics','scrollbars=yes,resizable=yes,width=300,height=390')
} */
</script>

<%--  <span class='gv_caption'>My Account</span><br /><br />
--%>
  <!-- -------------------------------------------------------------------------------------------- -->
  <!-- ** SQL DATA SOURCE ** -->
  <!-- ACCOUNT INFO -->
  <asp:SqlDataSource ID="SqlDataSourceUser" runat="server" ConnectionString="" 
    SelectCommand="EXEC spGetUserInfo @userID" SelectCommandType="Text" 
    UpdateCommand="EXEC spUpdateUserInfo @userID,@FirstName,@LastName,@Honorific,@Specialty,
      @Company,@Phone,@PhoneExt,@Fax,@PhoneOther, @Address1,@Address2,@City,
      @State,@ZipCode,@CountryID, @URL,@isAllowEmail" 
    UpdateCommandType="Text" >
    <SelectParameters>
      <asp:Parameter Name="userID" Type="Double" />
    </SelectParameters>
    <UpdateParameters>
      <asp:Parameter Name="UserID" Type="Double" />
      <asp:Parameter Name="txtProfFirstName" Type="String" />
      <asp:Parameter Name="txtProfLastName" Type="String" />
      <asp:Parameter Name="txtProfHonorific" Type="String" />
      <asp:Parameter Name="txtProfSpecialty" Type="String" />
      
      <asp:Parameter Name="txtProfCompany" Type="String" />
      <asp:Parameter Name="txtProfPhone" Type="String" />
      <asp:Parameter Name="txtProfPhoneExt" Type="String" />
      <asp:Parameter Name="txtProfFax" Type="String" />
      <asp:Parameter Name="txtProfAltPhone" Type="String" />

      <asp:Parameter Name="txtProfAddress1" Type="String" />
      <asp:Parameter Name="txtProfAddress2" Type="String" />
      <asp:Parameter Name="txtProfCity" Type="String" />

      <asp:Parameter Name="txtProfState" Type="String" />
      <asp:Parameter Name="txtProfZipCode" Type="String" />
      <asp:Parameter Name="ddlProfCountry" Type="Double" />

      <asp:Parameter Name="txtProfURL" Type="String" />
      <asp:Parameter Name="chkProfSubscribeEmail" Type="Byte" />
    </UpdateParameters>
  </asp:SqlDataSource>
  
  <!-- update email -->
  <asp:SqlDataSource ID="SqlDataSourceEmail"
    ConnectionString=""  runat="server" 
    SelectCommand="EXEC spGetUserInfo @userID" SelectCommandType="Text" 
    UpdateCommand="EXEC spUpdateEmailPwd '',@Email,@userID" UpdateCommandType="Text" >
    <SelectParameters>
      <asp:Parameter Name="userID" Type="Int32" />
    </SelectParameters>
    <UpdateParameters>
      <asp:Parameter Name="txtEmail" Type="String" />
      <asp:Parameter Name="UserID" Type="Int32"  />
      <asp:Parameter Name="ReturnCode" Type="Int32" Direction="ReturnValue" Size="4000" />
    </UpdateParameters>
  </asp:SqlDataSource>

  <!-- update password -->
  <asp:SqlDataSource ID="SqlDataSourcePassword" runat="server" ConnectionString="" 
    SelectCommand="EXEC spGetUserInfo @userID" SelectCommandType="Text" 
    InsertCommand="EXEC spUpdateEmailPwd @Pwd,'',@UserID" UpdateCommandType="Text" >
    <SelectParameters>
      <asp:Parameter Name="userID" Type="Double" />
    </SelectParameters>
    <InsertParameters>
      <asp:Parameter Name="txtPwd" Type="String" />
      <asp:Parameter Name="UserID" Type="Double" />
    </InsertParameters>
  </asp:SqlDataSource>

<%--@BillPhone,@BillPhoneExt,@BillFax,@BillPhoneOther,
--%>  
  <asp:SqlDataSource ID="SqlDataSourceBillUser" runat="server" ConnectionString="" 
    SelectCommand="EXEC spGetUserInfo @userID" SelectCommandType="Text" 
    UpdateCommand="EXEC spUpdateUserBillInfo @userID,@BillFirstName,@BillLastName,@BillHonorific,@BillCompany,
      @BillAddress1,@BillAddress2,@BillCity,@BillState,@BillZipCode,@BillCountryID" 
    UpdateCommandType="Text" >
    <SelectParameters>
      <asp:Parameter Name="userID" Type="Double" />
    </SelectParameters>
    <UpdateParameters>
      <asp:Parameter Name="UserID" Type="Double" />
      <asp:Parameter Name="txtBillProfFirstName" Type="String" />
      <asp:Parameter Name="txtBillProfLastName" Type="String" />
      <asp:Parameter Name="txtBillProfHonorific" Type="String" />
      
      <asp:Parameter Name="txtBillProfCompany" Type="String" />
      <asp:Parameter Name="txtBillProfAddress1" Type="String" />
      <asp:Parameter Name="txtBillProfAddress2" Type="String" />
      <asp:Parameter Name="txtBillProfCity" Type="String" />

      <asp:Parameter Name="txtBillProfState" Type="String" />
      <asp:Parameter Name="txtBillProfZipCode" Type="String" />
      <asp:Parameter Name="ddlBillProfCountry" Type="Double" />
    </UpdateParameters>
  </asp:SqlDataSource>

  <!-- Orders -->
  <asp:SqlDataSource ID="SqlDataSourceOrders" 
    ConnectionString="" SelectCommandType="Text" runat="server"
    SelectCommand="SELECT TOP 50 tblOrder.*,tblrOrderStatus.Description as OrderStatus FROM tblOrder 
      INNER JOIN tblrOrderStatus ON tblrOrderStatus.OrderStatusID = tblOrder.OrderStatusID
      WHERE (UserID=@UserID) ORDER BY OrderDate Desc" >
    <SelectParameters>
      <asp:Parameter Name="userID" Type="Double" />
    </SelectParameters>
  </asp:SqlDataSource>

  <!-- Country drop down list box -->
  <asp:SqlDataSource ID="SqlDataSourceCountry" 
    ConnectionString="" SelectCommandType="Text"
    SelectCommand="SELECT CountryID,Description FROM tblrCountry ORDER BY SortOrder"
    runat="server">
  </asp:SqlDataSource>
   
  <!-- State drop down list box -->
  <asp:SqlDataSource ID="SqlDataSourceStateCode" 
    ConnectionString="" SelectCommandType="Text"
    SelectCommand="SELECT StateCode,Description FROM tblrStateCode ORDER BY Description"
    runat="server">
  </asp:SqlDataSource>

  <!-- address type drop down list box -->
  <asp:SqlDataSource ID="SqlDataSourceAddressType" 
    ConnectionString="" SelectCommandType="Text"
    SelectCommand="SELECT * FROM tblrAddressType ORDER BY SortOrder"
    runat="server">
  </asp:SqlDataSource>

  <!-- ADDRESS UserID (multiple records) - Repeater template -->
  <asp:SqlDataSource ID="SqlDataSourceUserIDAddress" 
    ConnectionString="" runat="server"
    SelectCommand="spGetAddressInfo" SelectCommandType="StoredProcedure" 
    DeleteCommand="UPDATE tblAddress SET isDeleted=1 WHERE (AddressID = @AddressID)" DeleteCommandType="Text" >
    <SelectParameters>
      <asp:Parameter Name="addressID" Type="Double" />
      <asp:Parameter Name="userID" Type="Double" />
      <asp:Parameter Name="blnAll" Type="Boolean" />
    </SelectParameters>
    <DeleteParameters>
      <asp:Parameter Name="addressID" Type="Double" />
    </DeleteParameters>
  </asp:SqlDataSource>

  <!-- ADDRESS AdressID (1 record, Edit,Insert & Delete only) -->
  <asp:SqlDataSource ID="SqlDataSourceAddress" 
    ConnectionString="" runat="server" 
    SelectCommand="EXEC spGetAddressInfo @AddressID,@userID,@blnAll" SelectCommandType="Text"
    UpdateCommand="EXEC spUpdateAddressInfo @AddressID,@FirstName,@LastName,@Honorific,@Company,
      @Address1,@Address2,@City,@State,@Zip,@CountryID,@AddressTypeID" 
      UpdateCommandType="Text"
    InsertCommand="EXEC spInsertAddressInfo @UserID,@FirstName,@LastName,@Honorific,@Company,
      @Address1,@Address2,@City,@State,@Zip,@CountryID,@AddressTypeID" InsertCommandType="Text" >
    <SelectParameters>
      <asp:Parameter Name="addressID" Type="Double" />
      <asp:Parameter Name="userID" Type="Double" />
      <asp:Parameter Name="blnAll" Type="Boolean" />
    </SelectParameters>
    <UpdateParameters>
      <asp:Parameter Name="addressID" Type="Double" />
      <asp:Parameter Name="txtEditShipFirstName" Type="String" />
      <asp:Parameter Name="txtEditShipLastName" Type="String" />
      <asp:Parameter Name="txtEditShipHonorific" Type="String" />      
      <asp:Parameter Name="txtEditShipCompany" Type="String" />

      <asp:Parameter Name="txtEditShipAddress1" Type="String" />
      <asp:Parameter Name="txtEditShipAddress2" Type="String" />
      <asp:Parameter Name="txtEditShipCity" Type="String" />

      <asp:Parameter Name="txtEditShipState" Type="String" />
      <asp:Parameter Name="txtEditShipZip" Type="String" />
      <asp:Parameter Name="ddlEditShipCountry" Type="Double" />
      <asp:Parameter Name="ddlInsertAddressType" Type="Double" />
    </UpdateParameters>
    <InsertParameters>    
      <asp:Parameter Name="UserID" Type="Double" />
      <asp:Parameter Name="txtInsertShipFirstName" Type="String" />
      <asp:Parameter Name="txtInsertShipLastName" Type="String" />
      <asp:Parameter Name="txtInsertShipHonorific" Type="String" />      
      <asp:Parameter Name="txtInsertShipCompany" Type="String" />

      <asp:Parameter Name="txtInsertShipAddress1" Type="String" />
      <asp:Parameter Name="txtInsertShipAddress2" Type="String" />
      <asp:Parameter Name="txtInsertShipCity" Type="String" />

      <asp:Parameter Name="txtInsertShipState" Type="String" />
      <asp:Parameter Name="txtInsertShipZip" Type="String" />
      <asp:Parameter Name="ddlInsertShipCountry" Type="Double" />
      <asp:Parameter Name="ddlInsertAddressType" Type="Double" />
    </InsertParameters>
  </asp:SqlDataSource>

  <!-- -------------------------------------------------------------------------------------------- -->
  <!-- ** VALIDATION SUMMARY ** -->
  <asp:ValidationSummary ID="ValidationSummaryProfessional" runat="server" HeaderText="The following required fields are missing or have errors:"
    ValidationGroup="grpProf" ShowMessageBox="True" ShowSummary="false" DisplayMode="BulletList" />

  <asp:ValidationSummary ID="ValidationSummaryBillingProfessional" runat="server" HeaderText="The following required fields are missing or have errors:"
    ValidationGroup="grpBillProf" ShowMessageBox="True" ShowSummary="false" DisplayMode="BulletList" />

  <asp:ValidationSummary ID="ValidationSummaryShippingAddressEdit" runat="server" HeaderText="The following required fields are missing or have errors:"
    ValidationGroup="grpEditShip" ShowMessageBox="True" ShowSummary="false" DisplayMode="BulletList" />

  <asp:ValidationSummary ID="ValidationSummaryShippingAddressInsert" runat="server" HeaderText="The following required fields are missing or have errors:"
    ValidationGroup="grpInsertShip" ShowMessageBox="True" ShowSummary="false" DisplayMode="BulletList" />

  <asp:ValidationSummary ID="ValidationSummaryEditEmail" runat="server" HeaderText="The following required fields are missing or have errors:"
    ValidationGroup="grpEditEmail" ShowMessageBox="True" ShowSummary="false" DisplayMode="BulletList" />

  <!-- -------------------------------------------------------------------------------------------- -->
  <!-- ** TEMPLATES ** -->
  <asp:HiddenField ID="hdnIsSupportMaterials" runat="server" />
  
<%--              
  <asp:Panel ID="Panel2" runat="server" DefaultButton="btnUpdate">
  </asp:Panel>      
--%>            

  <table border="0" width="100%" cellspacing="5">
    <tr valign="top">
      <td rowspan="2">
      
        <!-- ----------------------------------------------------------------------------------------- -->
        <!-- CONTACT INFO -->
        <asp:Panel ID="pnlAccountInfo" runat="server" BorderStyle="None">
          
        <asp:FormView HeaderText="" ID="FormViewAccntInfo" 
            DataSourceID="SqlDataSourceUser" 
            DataKeyNames="UserID" 
            EmptyDataText="no general information" 
            runat="server" 
            CssClass="body_text" 
            GridLines="None" CellSpacing="0">
            
            <ItemTemplate>
              <asp:Button ID="btnDisableEnter" runat="server" CausesValidation="False" Enabled="False" 
                UseSubmitBehavior="True" Height="0px" Width="0px" style="display:none;" />

              <table width="250" border="0" >
                <tr><td><span class="menu_headings">My Account</span></td></tr>
                <tr><td>&nbsp;</td></tr>
                <tr>
                  <td align="left" class="checkout_header">Contact Information</td>
                  <td align="right"><asp:LinkButton ID="btnEdit" Text="Edit" CommandName="Edit" runat="server"></asp:LinkButton></td>
                </tr>
                <tr><td colspan="2"><hr /></td></tr>
              </table>
              <asp:Label ID="lblTstUserID" runat="server" Text="Label" Visible="false">UserID: <%#Eval("UserID")%><br /></asp:Label>
              Account Number: <%#Eval("MOMAccountNo")%><br /><br />
              
              <%#Eval("FirstName")%> <%#Eval("LastName")%><%#PrintHonorific(Eval("Honorific"))%><br />
              <%#PrintSpecialty(Eval("Specialty"), Eval("Degree"), Eval("LicenseNo"))%>
              <%#Eval("Company")%><br />
              <%#PrintAddressPart(Eval("Address1"), Eval("Address2"))%><br />
              <%#Eval("City")%> <%#Eval("State")%>, <%#Eval("ZipCode")%><br />
              <%#Eval("Country")%><br /><br />
              
              Phone: <%#Eval("Phone")%> Ext: <%#Eval("PhoneExt")%><br />
              Fax: <%#Eval("Fax")%><br />
              Alt Phone: <%#Eval("PhoneOther")%><br />
              <%#PrintLine("Website: ", Eval("URL"), "<br />")%>
              
              <asp:TextBox ID="txtNetTerms" runat="server" text='<%#Eval("isNetTerms") %>' Visible="false">
              </asp:TextBox>
              <br />
              Receive Email Updates: <%#PrintChkBoxAns(Eval("isAllowEmail"))%>
              <br /><br />
            </ItemTemplate>
            
            <EditItemTemplate>   
              <asp:Button ID="btnDisableEnter" runat="server" CausesValidation="False" Enabled="False" 
                UseSubmitBehavior="True" Height="0px" Width="0px" style="display:none;" />
                
              <table border="0" class="table_menu_indent">
                <tr><td><span class="menu_headings">Update My Account</span></td></tr>
                <tr><td class="notes_text" colspan="2">*Items marked with an astrisk are required.</td></tr>
                <tr><td colspan="2">&nbsp;</td></tr>
                <tr><td colspan="2"><b>General Information</b></td></tr>
                <tr><td colspan="2">&nbsp;</td></tr>
                <tr>
                  <td colspan="2">
                    Account Number: <%#Eval("MOMAccountNo")%>
                    <asp:Label ID="lblAuthCode" runat="server"><br />Authorization Code: <%#Eval("AuthCode")%></asp:Label>
                  </td>
                </tr>
                <tr><td colspan="2">&nbsp;</td></tr>
                <tr>
                  <td colspan="2">
                    First Name:<br />
                    <asp:TextBox ID='txtProfFirstName' Text='<%# Bind("FirstName") %>' runat='server' OnChange="javascript:setDirty();" ></asp:TextBox> *
                    <asp:RequiredFieldValidator ID="rfvProfFirstName" runat="server" ErrorMessage="First Name" Display="Dynamic"
                      ControlToValidate="txtProfFirstName" ValidationGroup="grpProf">Please enter your first name.</asp:RequiredFieldValidator>
                  </td>
                </tr>
                <tr>
                  <td colspan="2">
                    Last Name:<br />
                    <asp:TextBox ID='txtProfLastName' Text='<%# Bind("LastName") %>' runat='server' OnChange="javascript:setDirty();" ></asp:TextBox> *
                    <asp:RequiredFieldValidator ID="rfvProfLastName" runat="server" ErrorMessage="Last Name" Display="Dynamic"
                      ControlToValidate="txtProfLastName" ValidationGroup="grpProf">Please enter your last name.</asp:RequiredFieldValidator>
                  </td>
                </tr>
                <tr>
                  <td colspan="2">
                    <asp:Label ID="lblProfHonorific" runat="server" Text="Medical Designation/Title:<br />"></asp:Label>
                    <asp:TextBox ID='txtProfHonorific' Text='<%# Bind("Honorific") %>' runat='server' OnChange="javascript:setDirty();" ></asp:TextBox>
                    <asp:Label ID="lblProfHonorific2" runat="server" Text=" *"></asp:Label> 
<%--                    <asp:HyperLink ID="hlinkHonorific" NavigateUrl="" onclick="showHonorifics();"
                      runat="server" Font-Underline="True" >Approved Designations</asp:HyperLink>
--%>                    <asp:RequiredFieldValidator ID="rfvProfHonorific" runat="server" ErrorMessage="Medical Designation/Title" Display="Dynamic"
                      ControlToValidate="txtProfHonorific" ValidationGroup="grpProf">Please enter your medical designation/title.</asp:RequiredFieldValidator>
                  </td>
                </tr>
                <tr>
                  <td colspan="2">
                    <asp:Label ID="lblProfSpecialty" runat="server" Text="Specialty:<br />"></asp:Label>
                    <asp:TextBox ID='txtProfSpecialty' Text='<%# Bind("Specialty") %>' runat='server' OnChange="javascript:setDirty();" ></asp:TextBox>
                  </td>
                </tr>
                <tr>
                  <td colspan="2">
                    <!-- don't error check web address. Too many variables. -->
                    <asp:Label ID="lblProfURL" runat="server" Text="Web Address:<br />"></asp:Label>
                    <asp:TextBox ID='txtProfURL' Text='<%# Bind("URL") %>' runat='server' OnChange="javascript:setDirty();" ></asp:TextBox>
                  </td>
                </tr>
                <tr><td><asp:Label ID="lblTitle2" runat="server" Text="set in code"></asp:Label></td></tr>
                <tr>
                  <td colspan="2">
                    <asp:Label ID="lblProfCompany" runat="server" Text="Practice/Business Name:<br />"></asp:Label>
                    <asp:TextBox ID='txtProfCompany' Text='<%# Bind("Company") %>' runat='server' OnChange="javascript:setDirty();" ></asp:TextBox>
                  </td>
                </tr>
                <tr>
                  <td colspan="2">
                    Country:<br />
                    <asp:DropDownList ID='ddlProfCountry' runat='server' OnChange="javascript:setPostBack();" 
                      DataSourceID='SqlDataSourceCountry' DataTextField='Description' DataValueField='CountryID'
                      SelectedValue='<%# Bind("CountryID") %>'
                      OnTextChanged='ddlProfCountry_TextChanged' AutoPostBack='true'>
                    </asp:DropDownList> *

                    <asp:RequiredFieldValidator ID="rfvProfCountry" ControlToValidate="ddlProfCountry" Display="Dynamic"
                      InitialValue="--Select a Country--" runat="server" ErrorMessage="country required" ValidationGroup="grpProf">*
                    </asp:RequiredFieldValidator>
                    <asp:RangeValidator ControlToValidate="ddlProfCountry" ID="rvProfCountry" runat="server" Display="Dynamic"
                      ErrorMessage="Please pick a country" ValidationGroup="grpProf" MinimumValue="2" MaximumValue="99">Please select your country.</asp:RangeValidator>          
                  </td>
                </tr>
                <tr>
                  <td colspan="2">
                    Address Line 1:<br />
                    <asp:TextBox ID='txtProfAddress1' Text='<%# Bind("Address1") %>' runat='server' OnChange="javascript:setDirty();" ></asp:TextBox> *
                    <asp:RequiredFieldValidator ID="rfvProfAddress1" runat="server" ControlToValidate="txtProfAddress1"
                      ErrorMessage="Address Line 1" ValidationGroup="grpProf" Display="Dynamic">Please enter your address.</asp:RequiredFieldValidator>
                  </td>
                </tr>
                <tr>
                  <td colspan="2">
                    Address Line 2:<br />
                    <asp:TextBox ID='txtProfAddress2' Text='<%# Bind("Address2") %>' runat='server' OnChange="javascript:setDirty();" ></asp:TextBox>
                  </td>
                </tr>
                <tr>
                  <td colspan="2">
                    Zip/Postal Code:<br />
                    <asp:TextBox ID='txtProfZipCode' Text='<%# Bind("ZipCode") %>' runat='server' OnTextChanged="txtProfZipCode_TextChanged"
                      AutoPostBack="true" OnChange="javascript:setPostBack();" CausesValidation="true" ValidationGroup="grpProf" ></asp:TextBox> *
                      
                    <asp:RequiredFieldValidator ID="rfvProfZipCode" runat="server" ErrorMessage="Zip/Postal Code"
                      ControlToValidate="txtProfZipCode" ValidationGroup="grpProf" 
                      Display="Dynamic">Please enter a valid zip code.</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="revProfZipCode" runat="server" ErrorMessage="Zip/Postal Code"
                      ValidationGroup="grpProf" ControlToValidate="txtProfZipCode"
                      ValidationExpression="\d{5}(-\d{4})?" Display="Dynamic">Please enter a valid zip code.</asp:RegularExpressionValidator>
                  </td>
                </tr>
                <tr>
                  <td colspan="2">
                    City:<br />
                    <asp:TextBox ID='txtProfCity' Text='<%# Bind("City") %>' runat='server' OnChange="javascript:setDirty();" ></asp:TextBox> *
                    <asp:RequiredFieldValidator ID="rfvProfCity" runat="server" ErrorMessage="City" ControlToValidate="txtProfCity"
                      ValidationGroup="grpProf" Display="Dynamic">Please enter your city.</asp:RequiredFieldValidator>
                  </td>
                </tr>
                <tr>
                  <td colspan="2">
                    State/Province/Territory:<br />
                    <asp:TextBox ID='txtProfState' Text='<%# Bind("State") %>' runat='server'></asp:TextBox>
                      
                    <asp:DropDownList ID="ddlProfState" runat="server" DataSourceID='SqlDataSourceStateCode'
                      DataTextField='Description' DataValueField='StateCode'
                      OnTextChanged="ddlProfState_TextChanged" AutoPostBack="true" >
                    </asp:DropDownList> *

                    <asp:RequiredFieldValidator ID="rfvProfState" runat="server" ErrorMessage="State/Province/Territory"
                      ControlToValidate="txtProfState" ValidationGroup="grpProf" Display="Dynamic">
                      Please enter your State/Province/Territory.</asp:RequiredFieldValidator>
                  </td>
                </tr>
                <tr>
                  <td colspan="2">
                    <table border='0' cellpadding='0' cellspacing='0'>
                      <tr>
                        <td>
                          <asp:Label ID="lblBusPh" runat="server" Text="Business Phone:"></asp:Label>
                          <asp:Label ID="lblProfBusPhEx" runat="server" Text="Label">Ex: xxx-xxx-xxxx</asp:Label>
                          <br />
                          <asp:TextBox ID='txtProfPhone' Text='<%# Bind("Phone") %>' OnChange="javascript:setPostBack();"
                            runat='server' AutoPostBack="true" OnTextChanged="txtProfPhone_TextChanged" ></asp:TextBox> *
                        </td>
                        <td>&nbsp;</td>
                        <td>Ext:<br />
                          <asp:TextBox ID='txtProfExt' Text='<%# Bind("PhoneExt") %>' runat="server" MaxLength="10" Width="56px" 
                            OnChange="javascript:setDirty();"></asp:TextBox>
                          <asp:RequiredFieldValidator ID="rfvProfPhone" runat="server" ErrorMessage="Business Phone" Display="Dynamic"
                            ControlToValidate="txtProfPhone" ValidationGroup="grpProf">Please enter a valid phone number.</asp:RequiredFieldValidator>
                          <asp:RegularExpressionValidator ID="revProfPhone" runat="server" ValidationGroup="grpProf"
                            ControlToValidate="txtProfPhone" ErrorMessage="Business Phone" Display="Dynamic"
                            ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}|\d{10}">Please enter a valid phone number.</asp:RegularExpressionValidator>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr>
                  <td colspan="2">
                    <asp:Label ID="lblBusPax" runat="server" Text="Business Fax:"></asp:Label>
                    <asp:Label ID="lblProfBusPhFaxEx" runat="server" Text="Label">Ex: xxx-xxx-xxxx</asp:Label>
                    <br />
                    <asp:TextBox ID='txtProfFax' Text='<%# Bind("Fax") %>' OnChange="javascript:setPostBack();"
                      runat='server' AutoPostBack="true" OnTextChanged="txtProfFax_TextChanged" ></asp:TextBox>
                    <asp:RegularExpressionValidator ID="revProfFax" runat="server" ValidationGroup="grpProf"
                      ControlToValidate="txtProfFax" ErrorMessage="Fax Phone" 
                      ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}|\d{10}" Display="Dynamic">Please enter a valid phone number.</asp:RegularExpressionValidator>
                  </td>
                </tr>
                <tr>
                  <td colspan="2">
                    Alternate Phone: 
                    <asp:Label ID="lblProfAltPhEx" runat="server" Text="Ex: xxx-xxx-xxxx"></asp:Label>
                    <br />
                    <asp:TextBox ID='txtProfAltPhone' Text='<%# Bind("PhoneOther") %>' OnChange="javascript:setPostBack();"
                      runat='server' AutoPostBack="true" OnTextChanged="txtProfAltPhone_TextChanged" ></asp:TextBox>
                    <asp:RegularExpressionValidator ID="revProfAltPhone" runat="server" ValidationGroup="grpProf"
                      ControlToValidate="txtProfAltPhone" ErrorMessage="Alternate Phone" 
                      ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}|\d{10}" Display="Dynamic">Please enter a valid phone number.</asp:RegularExpressionValidator>
                  </td>
                </tr>
                <tr><td colspan="2">&nbsp;</td></tr>
                <tr><td colspan="2"><b>Subscribe to ProThera Emails</b></td></tr>
                <tr>
                  <td>
                    <asp:CheckBox ID="chkProfSubscribeEmail" runat="server" Checked='<%# Bind("isAllowEmail") %>' 
                      Text="Please email me important company and product announcements." /><br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="/forms/legal.aspx" target="_blank">Privacy Policy</a>
                  </td>
                  <td>&nbsp;</td>
                </tr>
                <tr><td colspan="2">&nbsp;</td></tr>
                <tr>
                  <td><asp:Button ID="btnCancel" Text="Cancel" CommandName="Cancel" runat="server" OnClientClick="javascript:warn_pageunload=true;" ></asp:Button></td>
                  <td><asp:Button ID="btnUpdate" Text="Update Account" CommandName="Update" runat="server" 
                    ValidationGroup="grpProf" OnClientClick="javascript:warn_pageunload=true;" ></asp:Button>
                  </td>
                </tr>
              </table>
            </EditItemTemplate>
          </asp:FormView>
        </asp:Panel>
        
        <!-- ----------------------------------------------------------------------------------------- -->
        <!-- BILLING INFORMATION -->
        <asp:Panel ID="pnlBillingInfo" runat="server" >
          <asp:Table ID="tblBillingNetTerms" Width="250" runat="server" Visible="false" CssClass="body_text">
            <asp:TableRow>
              <asp:TableCell CssClass="checkout_header">Credit Card Billing Information</asp:TableCell>
            </asp:TableRow>
            <asp:TableHeaderRow><asp:TableCell><hr /></asp:TableCell></asp:TableHeaderRow>
            <asp:TableHeaderRow><asp:TableCell>Net Terms</asp:TableCell></asp:TableHeaderRow>
            <asp:TableHeaderRow><asp:TableCell>&nbsp;</asp:TableCell></asp:TableHeaderRow>
          </asp:Table>            

          <asp:FormView ID="FormViewBillInfo" DataSourceID="SqlDataSourceBillUser" 
            DataKeyNames="UserID" EmptyDataText="no billing information" 
            runat="server" CssClass="body_text" GridLines="None" CellSpacing="0" Visible="false">
            
            <ItemTemplate>
              <asp:Button ID="btnDisableEnter" runat="server" CausesValidation="False" Enabled="False" 
                UseSubmitBehavior="True" Height="0px" Width="0px" style="display:none;" />

              <table width="250" border="0" >
                <tr>
                  <td align="left"><b>Credit Card Billing Information</b></td>
                  <td align="right">
                    <asp:LinkButton ID="btnEditBillInfo" Text="Edit" CommandName="Edit" runat="server">
                    </asp:LinkButton>
                  </td>
                </tr>
                <tr><td colspan="2"><hr /></td></tr>
              </table>
              
              <%#Eval("BillFirstName")%> <%#Eval("BillLastName")%><%#PrintHonorific(Eval("BillHonorific"))%><br />
              <%#Eval("BillCompany")%><br />
              <%#PrintAddressPart(Eval("BillAddress1"), Eval("BillAddress2"))%><br />
              <%#Eval("BillCity")%> <%#Eval("BillState")%>, <%#Eval("BillZipCode")%><br />
              <%#Eval("BillCountry")%><br /><br />
            </ItemTemplate>
            
            <EditItemTemplate>
              <asp:Button ID="btnDisableEnter" runat="server" CausesValidation="False" Enabled="False" 
                UseSubmitBehavior="True" Height="0px" Width="0px" style="display:none;" />

              <asp:Panel ID="Panel1" runat="server">
              <table border="0" class="table_menu_indent">
                <tr><td><span class="menu_headings">Update Credit Card Billing Information</span></td></tr>
                <tr><td class="notes_text" colspan="2">*Items marked with an astrisk are required.</td></tr>
                <tr><td colspan="2">&nbsp;</td></tr>
                <tr>
                  <td colspan="2">
                    First Name:<br />
                    <asp:TextBox ID='txtBillProfFirstName' Text='<%# Bind("BillFirstName") %>' runat='server' OnChange="javascript:setDirty();"></asp:TextBox> *
                    <asp:RequiredFieldValidator ID="rfvBillProfFirstName" runat="server" ErrorMessage="First Name" Display="Dynamic"
                      ControlToValidate="txtBillProfFirstName" ValidationGroup="grpBillProf">Please enter your first name.</asp:RequiredFieldValidator>
                  </td>
                </tr>
                <tr>
                  <td colspan="2">
                    Last Name:<br />
                    <asp:TextBox ID='txtBillProfLastName' Text='<%# Bind("BillLastName") %>' runat='server' OnChange="javascript:setDirty();"></asp:TextBox> *
                    <asp:RequiredFieldValidator ID="rfvBillProfLastName" runat="server" ErrorMessage="Bill Last Name" Display="Dynamic"
                      ControlToValidate="txtBillProfLastName" ValidationGroup="grpBillProf">Please enter your last name.</asp:RequiredFieldValidator>
                  </td>
                </tr>
                <tr>
                  <td colspan="2">
                    <asp:Label ID="lblBillProfHonorific" runat="server" Text="Medical Designation/Title:<br />"></asp:Label>
                    <asp:TextBox ID='txtBillProfHonorific' Text='<%# Bind("BillHonorific") %>' runat='server' OnChange="javascript:setDirty();" ></asp:TextBox>
                    <asp:Label ID="lblBillProfHonorific2" runat="server" Text=" *" ></asp:Label> 
<%--                    <asp:HyperLink ID="hlinkBillHonorific" NavigateUrl="" onclick="showHonorifics();"
                      runat="server" Font-Underline="True" >Approved Designations</asp:HyperLink>
--%>                    <asp:RequiredFieldValidator ID="rfvBillProfHonorific" runat="server" ErrorMessage="Medical Designation/Title" Display="Dynamic"
                      ControlToValidate="txtBillProfHonorific" ValidationGroup="grpBillProf">Please enter your medical designation/title.</asp:RequiredFieldValidator>
                  </td>
                </tr>
                <tr>
                  <td colspan="2">
                    <asp:Label ID="lblBillProfCompany" runat="server" Text="Practice/Business Name:<br />"></asp:Label>
                    <asp:TextBox ID='txtBillProfCompany' Text='<%# Bind("BillCompany") %>' runat='server' OnChange="javascript:setDirty();"></asp:TextBox>
                  </td>
                </tr>
                <tr>
                  <td colspan="2">
                    Country:<br />
                    <asp:DropDownList ID='ddlBillProfCountry' runat='server' OnChange="javascript:setPostBack();" 
                      DataSourceID='SqlDataSourceCountry' DataTextField='Description' DataValueField='CountryID'
                      OnTextChanged='ddlBillProfCountry_TextChanged' AutoPostBack='true'
                      SelectedValue='<%# Bind("BillCountryID") %>'>
                    </asp:DropDownList> *
                    
                    <asp:RequiredFieldValidator ID="rfvBillProfCountry" ControlToValidate="ddlBillProfCountry" Display="Dynamic"
                      InitialValue="--Select a Country--" runat="server" ErrorMessage="bill country required" ValidationGroup="grpBillProf">*
                    </asp:RequiredFieldValidator>
                    <asp:RangeValidator ControlToValidate="ddlBillProfCountry" ID="rvBillProfCountry" runat="server" Display="Dynamic"
                      ErrorMessage="country" ValidationGroup="grpBillProf" MinimumValue="2" MaximumValue="99">Please select your country.</asp:RangeValidator>          
                  </td>
                </tr>
                <tr>
                  <td colspan="2">
                    Address Line 1:<br />
                    <asp:TextBox ID='txtBillProfAddress1' Text='<%# Bind("BillAddress1") %>' runat='server' OnChange="javascript:setDirty();"></asp:TextBox> *
                    <asp:RequiredFieldValidator ID="rfvBillProfAddress1" runat="server" ControlToValidate="txtBillProfAddress1"
                      ErrorMessage="Address Line 1" ValidationGroup="grpBillProf" Display="Dynamic">Please enter your address.</asp:RequiredFieldValidator>
                  </td>
                </tr>
                <tr>
                  <td colspan="2">
                    Address Line 2:<br />
                    <asp:TextBox ID='txtBillProfAddress2' Text='<%# Bind("BillAddress2") %>' runat='server' OnChange="javascript:setDirty();"></asp:TextBox>
                  </td>
                </tr>
                <tr>
                  <td colspan="2">
                    Zip/Postal Code:<br />
                    <asp:TextBox ID='txtBillProfZipCode' Text='<%# Bind("BillZipCode") %>' runat='server' ValidationGroup="grpBillProf" 
                      AutoPostBack="true" OnTextChanged="txtBillProfZipCode_TextChanged" CausesValidation="true"
                      OnChange="javascript:setPostBack();" ></asp:TextBox> *
                      
                    <asp:RequiredFieldValidator ID="rfvBillProfZipCode" runat="server" ErrorMessage="Zip/Postal Code"
                      ControlToValidate="txtBillProfZipCode" ValidationGroup="grpBillProf" 
                      Display="Dynamic">Please enter a valid zip code.</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="revBillProfZipCode" runat="server" ErrorMessage="Zip/Postal Code"
                      ValidationGroup="grpBillProf" ControlToValidate="txtBillProfZipCode" 
                      ValidationExpression="\d{5}(-\d{4})?" Display="Dynamic">Please enter a valid zip code.</asp:RegularExpressionValidator>
                  </td>
                </tr>
                <tr>
                  <td colspan="2">
                    City:<br />
                    <asp:TextBox ID='txtBillProfCity' Text='<%# Bind("BillCity") %>' runat='server' OnChange="javascript:setDirty();"></asp:TextBox> *
                    <asp:RequiredFieldValidator ID="rfvBillProfCity" runat="server" ErrorMessage="City" ControlToValidate="txtBillProfCity"
                      ValidationGroup="grpBillProf" Display="Dynamic">Please enter your city.</asp:RequiredFieldValidator>
                  </td>
                </tr>
                <tr>
                  <td colspan="2">
                    State/Province/Territory:<br />
                    <asp:TextBox ID='txtBillProfState' Text='<%# Bind("BillState") %>' runat='server' OnChange="javascript:setDirty();"></asp:TextBox>
                    <asp:DropDownList ID="ddlBillProfState" runat="server" DataSourceID='SqlDataSourceStateCode'
                      DataTextField='Description' DataValueField='StateCode'
                      OnTextChanged="ddlBillProfState_TextChanged" AutoPostBack="true" >
                    </asp:DropDownList> *

                    <asp:RequiredFieldValidator ID="rfvBillProfState" runat="server" ErrorMessage="State/Province/Territory"
                      ControlToValidate="txtBillProfState" ValidationGroup="grpBillProf" Display="Dynamic">Please enter your State/Province/Territory.</asp:RequiredFieldValidator>
                  </td>
                </tr>
                <tr><td colspan="2">&nbsp;</td></tr>
                <tr><td colspan="2">&nbsp;</td></tr>
                <tr>
                  <td><asp:Button ID="btnCancel" Text="Cancel" CommandName="Cancel" runat="server" 
                    OnClientClick="javascript:warn_pageunload=true;"></asp:Button></td>
                  <td><asp:Button ID="btnUpdate" Text="Update Address" CommandName="Update" runat="server" 
                    ValidationGroup="grpBillProf" OnClientClick="javascript:warn_pageunload=true;"></asp:Button></td>
                </tr>                
              </table>
              </asp:Panel>
            </EditItemTemplate>
          </asp:FormView>
        </asp:Panel>
        
        <!-- ----------------------------------------------------------------------------------------- -->
        <!-- EMAIL / PASSWORD -->
        <asp:Panel ID="pnlPassword" runat="server" >
          <asp:FormView ID="FormViewEmailPassword" runat="server"
            DataSourceID="SqlDataSourceUser" 
            DataKeyNames="UserID" 
            EmptyDataText="no email/password information" 
            CssClass="body_text" 
            GridLines="None" CellSpacing="0">
          
            <ItemTemplate>
              <asp:Button ID="btnDisableEnter" runat="server" CausesValidation="False" Enabled="False" 
                UseSubmitBehavior="True" Height="0px" Width="0px" style="display:none;" />

              <table width="250" border="0" >
                <tr>
                  <td align="left" class="checkout_header">Email / Password</td>
                  <td align="right">
                    <asp:LinkButton ID="btnEditPwd" Text="Edit" CommandName="EditEmailPwd" runat="server">
                    </asp:LinkButton>
                  </td>
                </tr>
                <tr><td colspan="2"><hr /></td></tr>
              </table>
              <asp:TextBox ID="txtHiddenEmail" runat="server" Text='<%#Eval("email")%>' Visible="false"></asp:TextBox>
              Email Address: <%#Eval("Email")%><br />
              Password: ******<br /><br />
            </ItemTemplate>
          </asp:FormView>
                    
          <!-- ----------------------------------------------------------------------------------------- -->
          <!-- Edit Email -->
          <asp:Panel ID="pnlEditEmail" runat="server" Visible="false">
            <asp:Button ID="btnDisableEnter3" runat="server" CausesValidation="False" Enabled="False" 
              UseSubmitBehavior="True" Height="0px" Width="0px" style="display:none;" />

            <table border="0" >
              <tr><td colspan="2"><span class="menu_headings">Update Email / Password</span></td></tr>
              <tr><td class="notes_text" colspan="2">*Items marked with an astrisk are required.</td></tr>
              <tr><td colspan="2">&nbsp;</td></tr>
              <tr><td colspan="2">Update your email address and/or password below. 
                Changes will be effective immediately.</td></tr>
              <tr><td colspan="2">&nbsp;</td></tr>
<%--                  <tr><td colspan="2">UserID: <%# Eval("UserID") %></td></tr>
--%>                  
              <tr>
                <td width="200"><strong>Email Address:</strong></td>
                <td>
                  <asp:TextBox ID='txtEmail' Text='<%#Bind("Email") %>' runat='server' Width="250" MaxLength="50"></asp:TextBox> *
                  <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ErrorMessage="Email Address" Display="Dynamic"
                    ControlToValidate="txtEmail" ValidationGroup="grpEditEmail">Please enter a valid email address.</asp:RequiredFieldValidator>
                  <asp:RegularExpressionValidator ID="revEmail" runat="server" ErrorMessage="Email Address"
                    ControlToValidate="txtEmail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                    ValidationGroup="grpEditEmail" Display="Dynamic">Please enter a valid email address.</asp:RegularExpressionValidator>
                </td>
              </tr>    
              <tr><td colspan="2">&nbsp;</td></tr>
              <tr>
                <td><asp:Button ID="btnEmailCancel" Text="Cancel" CommandName="Cancel" runat="server"></asp:Button></td>
                <td><asp:Button ID="btnEmailUpdate" Text="Update Email" CommandName="Update" 
                  runat="server" ValidationGroup="grpEditEmail"></asp:Button></td>
              </tr>                
              <tr><td colspan="2">&nbsp;</td></tr>
            </table>
            <asp:Label ID="lblEmailMsg" runat="server" Text="Label" Visible="false" ForeColor="Red"></asp:Label>
          </asp:Panel>
                 

          <!-- ----------------------------------------------------------------------------------------- -->
          <!-- EDIT PASSWORD -->
          <asp:FormView ID="FormViewEditPwd" runat="server"
            DataSourceID="SqlDataSourcePassword" DataKeyNames="UserID" EmptyDataText="no password"
            CssClass="body_text" GridLines="None" DefaultMode="Insert" Visible="false">
            <ItemTemplate>
              UserID: <%#Eval("UserID") %><br />
              Password<br />
            </ItemTemplate>          
            <InsertItemTemplate>
              <asp:Panel ID="Panel3" runat="server" DefaultButton="btnPwdUpdate">
                <table border="0" >
                  <tr><td colspan="2">&nbsp;</td></tr>
                  <tr>
                    <td width="200"><b>New Password:</b></td>
                    <td>
                      <asp:TextBox ID="txtPwd" Text='<%#Bind("Pwd") %>' runat="server" TextMode="Password" ToolTip="enter new password"></asp:TextBox>
                      <asp:RequiredFieldValidator ID="rfvPassword" runat="server" ErrorMessage="Password"
                        ControlToValidate="txtPwd" ValidationGroup="grpEditPwd" 
                        Display="Dynamic">Please enter a password.</asp:RequiredFieldValidator>
                      <asp:CustomValidator ID="cvPassword" runat="server" ErrorMessage="Password length must be 6-15 characters in length. Alpha-numeric characters only."
                        ControlToValidate="txtPwd" ValidationGroup="grpEditPwd" OnServerValidate="cvPassword_ServerValidate"
                        Display="Dynamic">*</asp:CustomValidator>
                      <asp:Label ID="lblPwdMsg" runat="server" Text="Label"></asp:Label>
                    </td>
                  </tr>
                  <tr>
                    <td><b>Confirm New Password:</b></td>
                    <td>
                      <asp:TextBox ID="txtPwdConfirm" runat="server" TextMode="Password" ToolTip="confirm new password"></asp:TextBox>
                      <asp:CompareValidator ID="CompValidPasswords" runat="server" ControlToValidate="txtPwd"
                        ErrorMessage="Passwords do not match. Please try again." Type="String" Operator="Equal" ControlToCompare="txtPwdConfirm"
                        ValidationGroup="grpEditPwd" Display="Dynamic" SetFocusOnError="True">*</asp:CompareValidator>
                    </td>
                  </tr>
                  <tr><td colspan="2">&nbsp;</td></tr>
                  <tr>
                    <td><asp:Button ID="btnPwdCancel" Text="Cancel" CommandName="Cancel" runat="server"></asp:Button></td>
                    <td><asp:Button ID="btnPwdUpdate" Text="Update Password" CommandName="Insert" 
                      runat="server" ValidationGroup="grpEditPwd"></asp:Button></td>
                  </tr>                
                </table>
              </asp:Panel>
            </InsertItemTemplate>
          </asp:FormView>
            
          <asp:ValidationSummary ID="ValidationSummaryEditPwd" runat="server" HeaderText=""
            ValidationGroup="grpEditPwd" ShowSummary="true" DisplayMode="List" />
        </asp:Panel>
        
        <!-- ----------------------------------------------------------------------------------------- -->
        <!-- SUPPORT MATERIALS -->
        <asp:Panel ID="pnlSupportMaterials" runat="server" >
          <table width="250" border="0" class="body_text">
            <tr>
              <td align="left" class="checkout_header">Support Materials</td>
              <td align="right">
                <a href="ShowDocsSupport.aspx" >View</a>
              </td>
            </tr>
            <tr><td colspan="2"><hr /></td></tr>
            <tr>
              <td>
                Access product support materials customized for your account here.<br /><br />
                General information such as Technical Articles, Monographs, and ProThera<span class="superscript">&reg;</span>
                and Klaire Labs<span class="superscript">&reg;</span> brand Patient Handouts can be accessed 
                under 'Technical Info' in the left-hand column of each page.              
              </td>
            </tr>
          </table>
        </asp:Panel>
      </td>
  
      <td>
        <!-- ----------------------------------------------------------------------------------------- -->
        <!-- ORDER HISTORY (multiple records) -->
        <asp:Panel ID="pnlOrders" runat="server">
          <br />
          <table width="250" border="0" class="body_text">
            <tr>
              <td align="left" class="checkout_header">Order History</td>
              <td align="right">
                <a href="ShowOrderHistory.aspx" >See All Orders</a>
              </td>
            </tr>
            <tr><td colspan="2"><hr /></td></tr>
            <tr>
              <td colspan="2">
              
          <asp:GridView ID="gvOrders" runat="server" AutoGenerateColumns="False" 
            DataSourceID="SqlDataSourceOrders" AllowPaging="True" AllowSorting="True" 
            DataKeyNames="OrderID" EmptyDataText="No order to display" PageSize="5" 
            HeaderStyle-BackColor="#2D5BA9" HeaderStyle-ForeColor="White" 
            CssClass="body_text" 
            CaptionAlign="Top" CellPadding="5" CellSpacing="1" BorderStyle="None"
            GridLines="None" Width="100%">
            <RowStyle BackColor="#D6E6F6" />
            <Columns>
              <asp:HyperLinkField HeaderText="Order" DataNavigateUrlFields="OrderID" 
                DataNavigateUrlFormatString="~/myAccountOrderDetails.aspx?ID={0}" DataTextField="OrderNumber" 
                SortExpression="OrderNumber"  />
              <asp:BoundField HeaderText="Order Date" DataField="OrderDate" 
                DataFormatString="{0:MM/dd/yyyy}" SortExpression="OrderDate" 
                HtmlEncodeFormatString="true" ConvertEmptyStringToNull="False"/>
              <asp:BoundField HeaderText="Status" DataField="OrderStatus" 
                HtmlEncodeFormatString="true" ConvertEmptyStringToNull="False"/>
            </Columns>
            
            <SelectedRowStyle BackColor="Gray" />
            <HeaderStyle BackColor="#2D5BA9" ForeColor="White" />
            <AlternatingRowStyle BackColor="#EBF3FB" />
          </asp:GridView>
              
              </td>
            </tr>
          </table>
        
        </asp:Panel>
      </td>
    </tr>
    
    <tr>
      <td>
        <!-- ----------------------------------------------------------------------------------------- -->
        <!-- SHIPPING ADDRESSES (multiple records) -->
        <asp:Panel ID="pnlShipping" runat="server" BorderStyle="None" >
          <asp:DataList ID="DataListShipping" runat="server" DataSourceID="SqlDataSourceUserIDAddress" 
            RepeatColumns="1" style="vertical-align" BorderStyle="None"
            DataKeyNames="addressID" CssClass="body_text" >
              <HeaderTemplate>
                <table border="0" width="250" class="body_text">
                  <tr><td align="left" colspan="4" class="checkout_header"><br />Shipping Address(es)</td></tr>
                  <tr><td colspan="4"><hr /></td></tr>
                  <tr>
                    <td colspan="4">
                      <asp:LinkButton ID='lnkBtnShipAdd' runat='server' CommandName="AddNewAddress" 
                        CommandArgument='<%#Eval("UserID") %>' >Add New Address</asp:LinkButton>
                      <asp:Label ID="lblShipMax" runat="server">(Up to <%#SHIPPING_ADDRESS_MAX %> total)</asp:Label>
                    </td>
                    <tr><td colspan="4">&nbsp;</td></tr>
                  </tr>
              </HeaderTemplate>
              <FooterTemplate>
                </table>
              </FooterTemplate>
              <ItemTemplate>
                  <tr>
                    <td><strong>Address <%#Container.ItemIndex + 1%>: <%#Eval("AddressType")%></strong></td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>
                      <asp:LinkButton ID='lnkBtnShipEdit' runat='server' CommandName="UpdateAddress" 
                        CommandArgument='<%#Eval("AddressID") %>' >Edit</asp:LinkButton>
                        &nbsp;&nbsp;&nbsp;
                      <asp:LinkButton ID='lnkBtnShipDelete' runat='server' CommandName="DeleteAddress" 
                        OnClientClick="return confirm('Delete Shipping Address\n\n Are you sure you want to delete this address?')"
                        CommandArgument='<%#Eval("AddressID") %>' >Delete</asp:LinkButton>
                    </td>
                  </tr>
                  <tr>
                    <td colspan="4">
                      <%#Eval("FirstName")%> <%#Eval("LastName")%>, <%#Eval("Honorific")%><br />
                      <%#Eval("Company")%><br />
                      <%#PrintAddressPart(Eval("Address1"), Eval("Address2"))%><br />
                      <%#Eval("City")%>, <%#Eval("State")%> <%#Eval("Zip")%><br />
                      <%#Eval("Country")%>
                </td>
                  </tr>
              </ItemTemplate>
              <SeparatorTemplate><tr><td>&nbsp;</td></tr></SeparatorTemplate>
          </asp:DataList>
          <asp:TextBox ID="txtShipAddressCount" runat="server" Visible="false"></asp:TextBox>
        
          <!-- ----------------------------------------------------------------------------------------- -->
          <!-- SHIPPING ADDRESS - Edit/Insert -->
<%--          <asp:Panel ID="Panel4" runat="server" DefaultButton="lnkbtnShipInsUpdate">
          </asp:Panel>           

              <asp:Panel ID="Panel5" runat="server" DefaultButton="lnkbtnShipUpdate">
              </asp:Panel>
          <asp:RangeValidator ControlToValidate="ddlInsertShipCountry" ID="rvInsertShipCountry" runat="server" Display="Dynamic"
            ErrorMessage="country" ValidationGroup="grpInsertShip" MinimumValue="2" MaximumValue="99">Please select your country.</asp:RangeValidator>          
              
--%>
          <asp:FormView ID="FormViewAddress" DataSourceID="SqlDataSourceAddress" DataKeyNames="AddressID" 
            EmptyDataText="no shipping address information" runat="server" 
            CssClass="body_text" GridLines="None" DefaultMode="Edit" Visible="false">            
            <HeaderTemplate></HeaderTemplate>
            <FooterTemplate></FooterTemplate>
          
            <ItemTemplate>
              <asp:Button ID="btnDisableEnter3" runat="server" CausesValidation="False" Enabled="False" 
                UseSubmitBehavior="True" Height="0px" Width="0px" style="display:none;" />

              <asp:LinkButton ID="btnShipEdit" Text="edit" CommandName="Edit" runat="server"></asp:LinkButton>
              <br /><br />

              <%#Eval("FirstName")%> <%#Eval("LastName")%>, <%#Eval("Honorific")%><br />
              <%#Eval("Company")%><br />
              <%#PrintAddressPart(Eval("Address1"), Eval("Address2"))%><br />
              <%#Eval("City")%> <%#Eval("State")%>, <%#Eval("Zip")%><br />
              <%#Eval("Country")%><br /><br />
              
              <%#Eval("Company")%><br />
              <%#Eval("Phone")%><br />
              Fax: <%#Eval("Fax")%><br />
              <%#Eval("Email")%><br />
            </ItemTemplate>
            
            <InsertItemTemplate>
              <asp:Button ID="btnDisableEnter3" runat="server" CausesValidation="False" Enabled="False" 
                UseSubmitBehavior="True" Height="0px" Width="0px" style="display:none;" />

              <table border="0" class="table_menu_indent">
                <tr><td><span class="menu_headings">Add Shipping Address</span></td></tr>
                <tr><td class="notes_text" colspan="2">*Items marked with an astrisk are required.</td></tr>
                <tr><td colspan="2">&nbsp;</td></tr>
                <tr>
                  <td colspan="2">
                    <strong>Name this Address:</strong> (For your reference only. This name will 
                    not appear on the shipping label.)<br /><br />
                    <asp:DropDownList ID="ddlAddressType" runat="server" DataSourceID='SqlDataSourceAddressType' 
                      DataTextField='Description' DataValueField='AddressTypeID' 
                      SelectedValue='<%# Bind("AddressTypeID") %>' ></asp:DropDownList> *
                      
                    <asp:RequiredFieldValidator ID="rfvAddressType" ControlToValidate="ddlAddressType" 
                      Display="Dynamic" InitialValue="-- Select One --" runat="server" 
                      ErrorMessage="address type" ValidationGroup="grpInsertShip">Please select your address type.
                    </asp:RequiredFieldValidator>
                    <asp:RangeValidator ControlToValidate="ddlAddressType" ID="rvAddressType" runat="server" 
                      Display="Dynamic" ErrorMessage="address type" ValidationGroup="grpInsertShip" 
                      MinimumValue="2" MaximumValue="99">Please select your address type.</asp:RangeValidator>
                  </td>
                </tr>
                <tr><td colspan="2">&nbsp;</td></tr>
                <tr><td colspan="2"><strong>Shipping Information</strong></td></tr>
                <tr><td colspan="2">&nbsp;</td></tr>
                <tr>
                  <td colspan="2">
                    <asp:TextBox ID='txtUserID' Text='<%# Bind("UserID") %>' runat='server' Visible="false" ></asp:TextBox>
                    First Name:<br />
                    <asp:TextBox ID='txtInsertShipFirstName' Text='<%# Bind("FirstName") %>' runat='server' OnChange="javascript:setDirty();"></asp:TextBox> *
                    <asp:RequiredFieldValidator ID="rfvInsertShipFirstName" runat="server" ErrorMessage="First Name" Display="Dynamic"
                      ControlToValidate="txtInsertShipFirstName" ValidationGroup="grpInsertShip">Please enter your first name.</asp:RequiredFieldValidator>
                  </td>
                </tr>
                <tr>
                  <td colspan="2">
                    Last Name:<br />
                    <asp:TextBox ID='txtInsertShipLastName' Text='<%# Bind("LastName") %>' runat='server' OnChange="javascript:setDirty();"></asp:TextBox> *
                    <asp:RequiredFieldValidator ID="rfvInsertShipLastName" runat="server" ErrorMessage="Bill Last Name" Display="Dynamic"
                      ControlToValidate="txtInsertShipLastName" ValidationGroup="grpInsertShip">Please enter your last name.</asp:RequiredFieldValidator>
                  </td>
                </tr>
                <tr>
                  <td colspan="2">
                    <asp:Label ID="lblInsertShipHonorific" runat="server" Text="Medical Designation/Title:<br />"></asp:Label>
                    <asp:TextBox ID='txtInsertShipHonorific' Text='<%# Bind("Honorific") %>' runat='server' OnChange="javascript:setDirty();"></asp:TextBox>
                    <asp:Label ID="lblInsertShipHonorific2" runat="server" Text=" *"></asp:Label> 
<%--                    <asp:HyperLink ID="hlinkInsertShipHonorific" NavigateUrl="" onclick="showHonorifics();"
                      runat="server" Font-Underline="True" >Approved Designations</asp:HyperLink>
--%>                    <asp:RequiredFieldValidator ID="rfvInsertShipHonorific" runat="server" ErrorMessage="Medical Designation/Title" Display="Dynamic"
                      ControlToValidate="txtInsertShipHonorific" ValidationGroup="grpInsertShip">Please enter your medical designation/title.</asp:RequiredFieldValidator>
                  </td>
                </tr>
                <tr>
                  <td colspan="2">
                    <asp:Label ID="lblInsertShipCompany" runat="server" Text="Practice/Business Name:<br />"></asp:Label>
                    <asp:TextBox ID='txtInsertShipCompany' Text='<%# Bind("Company") %>' runat='server' OnChange="javascript:setDirty();"></asp:TextBox>
                  </td>
                </tr>
                <tr>
                  <td colspan="2">
                    Country:<br />
                    <asp:DropDownList ID='ddlInsertShipCountry' runat='server' OnChange="javascript:setPostBack();"
                      DataSourceID='SqlDataSourceCountry' DataTextField='Description' DataValueField='CountryID'
                      SelectedValue='<%# Bind("CountryID") %>'
                      OnTextChanged='ddlInsertShipCountry_TextChanged' AutoPostBack='true'>
                    </asp:DropDownList> *
                    
                    <asp:RequiredFieldValidator ID="rfvInsertShipCountry" ControlToValidate="ddlInsertShipCountry" Display="Dynamic"
                      InitialValue="--Select a Country--" runat="server" ErrorMessage="bill country required" ValidationGroup="grpInsertShip">*
                    </asp:RequiredFieldValidator>
                  </td>
                </tr>
                <tr>
                  <td colspan="2">
                    Address Line 1:<br />
                    <asp:TextBox ID='txtInsertShipAddress1' Text='<%# Bind("Address1") %>' runat='server' OnChange="javascript:setDirty();"></asp:TextBox> *
                    <asp:RequiredFieldValidator ID="rfvInsertShipAddress1" runat="server" ControlToValidate="txtInsertShipAddress1"
                      ErrorMessage="Address Line 1" ValidationGroup="grpInsertShip" Display="Dynamic">Please enter your address.</asp:RequiredFieldValidator>
                  </td>
                </tr>
                <tr>
                  <td colspan="2">
                    Address Line 2:<br />
                    <asp:TextBox ID='txtInsertShipAddress2' Text='<%# Bind("Address2") %>' runat='server' OnChange="javascript:setDirty();"></asp:TextBox>
                  </td>
                </tr>
                <tr>
                  <td colspan="2">
                    Zip/Postal Code:<br />
                    <asp:TextBox ID='txtInsertShipZipCode' Text='<%# Bind("Zip") %>' runat='server' 
                      OnChange="javascript:setPostBack();" 
                      AutoPostBack="true" OnTextChanged="txtInsertShipZipCode_TextChanged"></asp:TextBox> *

                    <asp:RequiredFieldValidator ID="rfvInsertShipZipCode" runat="server" ErrorMessage="Zip/Postal Code"
                      ControlToValidate="txtInsertShipZipCode" ValidationGroup="grpInsertShip" 
                      Display="Dynamic">Please enter a valid zip code.</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="revInsertShipZipCode" runat="server" ErrorMessage="Zip/Postal Code"
                      ValidationGroup="grpInsertShip" ControlToValidate="txtInsertShipZipCode" 
                      ValidationExpression="\d{5}(-\d{4})?" Display="Dynamic">Please enter a valid zip code.</asp:RegularExpressionValidator>
                  </td>
                </tr>
                <tr>
                  <td colspan="2">
                    City:<br />
                    <asp:TextBox ID='txtInsertShipCity' Text='<%# Bind("City") %>' runat='server'></asp:TextBox> *
                    <asp:RequiredFieldValidator ID="rfvInsertShipCity" runat="server" ErrorMessage="City" ControlToValidate="txtInsertShipCity"
                      ValidationGroup="grpInsertShip" Display="Dynamic">Please enter your city.</asp:RequiredFieldValidator>
                  </td>
                </tr>
                <tr>
                  <td colspan="2">
                    STATE/Province/Territory:<br />
                    <asp:TextBox ID='txtInsertShipState' Text='<%# Bind("State") %>' runat='server'></asp:TextBox>
                    <asp:DropDownList ID="ddlInsertShipState" runat="server" DataSourceID='SqlDataSourceStateCode'
                      DataTextField='Description' DataValueField='StateCode'
                      OnTextChanged="ddlInsertShipState_TextChanged" AutoPostBack="true" >
                    </asp:DropDownList> *

                    <!-- TextBox validator -->
                    <asp:RequiredFieldValidator ID="rfvInsertShipState" runat="server" ErrorMessage="State/Province/Territory"
                      ControlToValidate="txtInsertShipState" ValidationGroup="grpInsertShip" Enabled="true"
                      Display="Dynamic">Please enter your State/Province/Territory.</asp:RequiredFieldValidator>                      

                    <!-- DDL validator -->
                    <asp:RequiredFieldValidator ID="rfvInsertShipStateDDL" runat="server" ErrorMessage="State/Province/Territory"
                      ControlToValidate="ddlInsertShipState" ValidationGroup="grpInsertShip" Display="Dynamic" 
                      InitialValue="">Please enter your State/Province/Territory.</asp:RequiredFieldValidator>
                  </td>
                </tr>
              </table>
              
              <br /><br />
              <asp:Button ID="lnkBtnShipInsCancel" Text="Cancel" CommandName="Cancel" runat="server" 
                OnClientClick="javascript:warn_pageunload=true;" ></asp:Button>
              &nbsp;&nbsp;
              <asp:Button ID="lnkbtnShipInsUpdate" Text="Add Address" CommandName="Insert" runat="server" 
                ValidationGroup="grpInsertShip" OnClientClick="javascript:warn_pageunload=true;"></asp:Button>
            </InsertItemTemplate>

<%--<br />AddressID: <%#Eval("AddressID")%><br />
--%>            
            <EditItemTemplate>
              <asp:Button ID="btnDisableEnter3" runat="server" CausesValidation="False" Enabled="False" 
                UseSubmitBehavior="True" Height="0px" Width="0px" style="display:none;" />

              <table border="0" class="table_menu_indent">
                <tr><td><span class="menu_headings">Update Shipping Address</span></td></tr>
                <tr><td>&nbsp;</td></tr>
                <tr><td class="notes_text" colspan="2">*Items marked with an astrisk are required.</td></tr>
                <tr><td colspan="2">&nbsp;</td></tr>
                <tr>
                  <td colspan="2">
                    <strong>Name this Address:</strong> (For your reference only. This name will 
                    not appear on the shipping label.)<br /><br />
                    <asp:DropDownList ID="ddlEditAddressType" runat="server" DataSourceID='SqlDataSourceAddressType' 
                      DataTextField='Description' DataValueField='AddressTypeID' ValidationGroup="grpEditShip"
                      SelectedValue='<%# Bind("AddressTypeID") %>' ></asp:DropDownList> *
                      
                    <asp:RequiredFieldValidator ID="rfvEditAddressType" ControlToValidate="ddlEditAddressType" 
                      Display="Dynamic" InitialValue="-- Select One --" runat="server" 
                      ErrorMessage="address type" ValidationGroup="grpEditShip">Please select your address type.
                    </asp:RequiredFieldValidator>
                    <asp:RangeValidator ControlToValidate="ddlEditAddressType" ID="rvEditAddressType" runat="server" 
                      Display="Dynamic" ErrorMessage="address type" ValidationGroup="grpEditShip" 
                      MinimumValue="2" MaximumValue="99">Please select your address type.</asp:RangeValidator>
                    &nbsp;&nbsp;
                  </td>
                </tr>
                <tr><td colspan="2">&nbsp;</td></tr>
                <tr>
                  <td colspan="2">
                    First Name:<br />
                    <asp:TextBox ID='txtEditShipFirstName' Text='<%# Bind("FirstName") %>' runat='server' OnChange="javascript:setDirty();" ></asp:TextBox> *
                    <asp:RequiredFieldValidator ID="rfvEditShipFirstName" runat="server" ErrorMessage="First Name" Display="Dynamic"
                      ControlToValidate="txtEditShipFirstName" ValidationGroup="grpEditShip">Please enter your first name.</asp:RequiredFieldValidator>
                  </td>
                </tr>
                <tr>
                  <td colspan="2">
                    Last Name:<br />
                    <asp:TextBox ID='txtEditShipLastName' Text='<%# Bind("LastName") %>' runat='server' OnChange="javascript:setDirty();" ></asp:TextBox> *
                    <asp:RequiredFieldValidator ID="rfvEditShipLastName" runat="server" ErrorMessage="Bill Last Name" Display="Dynamic"
                      ControlToValidate="txtEditShipLastName" ValidationGroup="grpEditShip">Please enter your last name.</asp:RequiredFieldValidator>
                  </td>
                </tr>
                <tr>
                  <td colspan="2">
                    <asp:Label ID="lblEditShipHonorific" runat="server" Text="Medical Designation/Title:<br />"></asp:Label>
                    <asp:TextBox ID='txtEditShipHonorific' Text='<%# Bind("Honorific") %>' runat='server' OnChange="javascript:setDirty();" ></asp:TextBox>
                    <asp:Label ID="lblEditShipHonorific2" runat="server" Text=" *"></asp:Label> 
<%--                    <asp:HyperLink ID="hlinkEditShipHonorific" NavigateUrl="" onclick="showHonorifics();"
                      runat="server" Font-Underline="True" >Approved Designations</asp:HyperLink>
--%>                    <asp:RequiredFieldValidator ID="rfvEditShipHonorific" runat="server" ErrorMessage="Medical Designation/Title" Display="Dynamic"
                      ControlToValidate="txtEditShipHonorific" ValidationGroup="grpEditShip">Please enter your medical designation/title.</asp:RequiredFieldValidator>
                  </td>
                </tr>
                <tr>
                  <td colspan="2">
                    <asp:Label ID="lblEditShipCompany" runat="server" Text="Practice/Business Name:<br />"></asp:Label>
                    <asp:TextBox ID='txtEditShipCompany' Text='<%# Bind("Company") %>' runat='server' OnChange="javascript:setDirty();" ></asp:TextBox>
                  </td>
                </tr>
                <tr>
                  <td colspan="2">
                    Country:<br />
                    <asp:DropDownList ID='ddlEditShipCountry' runat='server'
                      DataSourceID='SqlDataSourceCountry' DataTextField='Description' DataValueField='CountryID'
                      SelectedValue='<%# Bind("CountryID") %>'
                      OnTextChanged='ddlEditShipCountry_TextChanged' AutoPostBack='true'>
                    </asp:DropDownList> *
                    
                    <asp:RequiredFieldValidator ID="rfvEditShipCountry" ControlToValidate="ddlEditShipCountry" Display="Dynamic"
                      InitialValue="--Select a Country--" runat="server" ErrorMessage="bill country required" ValidationGroup="grpEditShip">*
                    </asp:RequiredFieldValidator>
                    <asp:RangeValidator ControlToValidate="ddlEditShipCountry" ID="rvEditShipCountry" runat="server" Display="Dynamic"
                      ErrorMessage="country" ValidationGroup="grpEditShip" MinimumValue="2" MaximumValue="99">Please select your country.</asp:RangeValidator>          
                  </td>
                </tr>
                <tr>
                  <td colspan="2">
                    Address Line 1:<br />
                    <asp:TextBox ID='txtEditShipAddress1' Text='<%# Bind("Address1") %>' runat='server' OnChange="javascript:setDirty();" ></asp:TextBox> *
                    <asp:RequiredFieldValidator ID="rfvEditShipAddress1" runat="server" ControlToValidate="txtEditShipAddress1"
                      ErrorMessage="Address Line 1" ValidationGroup="grpEditShip" Display="Dynamic">Please enter your address.</asp:RequiredFieldValidator>
                  </td>
                </tr>
                <tr>
                  <td colspan="2">
                    Address Line 2:<br />
                    <asp:TextBox ID='txtEditShipAddress2' Text='<%# Bind("Address2") %>' runat='server' OnChange="javascript:setDirty();" ></asp:TextBox>
                  </td>
                </tr>
                <tr>
                  <td colspan="2">
                    Zip/Postal Code:<br />
                    <asp:TextBox ID='txtEditShipZipCode' Text='<%# Bind("Zip") %>' runat='server' OnTextChanged="txtEditShipZipCode_TextChanged"
                      AutoPostBack="true" CausesValidation="true" ValidationGroup="grpEditShip" OnChange="javascript:setPostBack();" ></asp:TextBox> *
                      
                    <asp:RequiredFieldValidator ID="rfvEditShipZipCode" runat="server" ErrorMessage="Zip/Postal Code"
                      ControlToValidate="txtEditShipZipCode" ValidationGroup="grpEditShip" 
                      Display="Dynamic">Please enter a valid zip code.</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="revEditShipZipCode" runat="server" ErrorMessage="Zip/Postal Code"
                      ValidationGroup="grpEditShip" ControlToValidate="txtEditShipZipCode" 
                      ValidationExpression="\d{5}(-\d{4})?" Display="Dynamic">Please enter a valid zip code.</asp:RegularExpressionValidator>
                  </td>
                </tr>
                <tr>
                  <td colspan="2">
                    City:<br />
                    <asp:TextBox ID='txtEditShipCity' Text='<%# Bind("City") %>' runat='server' ></asp:TextBox> *
                    <asp:RequiredFieldValidator ID="rfvEditShipCity" runat="server" ErrorMessage="City" ControlToValidate="txtEditShipCity"
                      ValidationGroup="grpEditShip" Display="Dynamic">Please enter your city.</asp:RequiredFieldValidator>
                  </td>
                </tr>
                <tr>
                  <td colspan="2">
                    State/Province/Territory:<br />
                    <asp:TextBox ID='txtEditShipState' Text='<%# Bind("State") %>' runat='server' CausesValidation="True" ></asp:TextBox>
                    
                    <asp:DropDownList ID="ddlEditShipState" runat="server" DataSourceID='SqlDataSourceStateCode'
                      DataTextField='Description' DataValueField='StateCode' 
                      OnTextChanged="ddlEditShipState_TextChanged" AutoPostBack="true" >
                    </asp:DropDownList> *
                    <asp:RequiredFieldValidator ID="rfvEditShipState" runat="server" ErrorMessage="State/Province/Territory"
                      ControlToValidate="txtEditShipState" ValidationGroup="grpEditShip" Enabled="true"
                      Display="Dynamic">Please enter your State/Province/Territory.</asp:RequiredFieldValidator>
                  </td>
                </tr>
              </table>
              
              <br /><br />
              <asp:Button ID="lnkBtnShipCancel" Text="Cancel" CommandName="Cancel" runat="server" OnClientClick="javascript:warn_pageunload=true;" ></asp:Button>
              &nbsp;&nbsp;
              <%-- <asp:Button ID="lnkBtnShipDelete" Text="Delete" CommandName="Delete" runat="server" 
                OnClientClick="return confirm('Delete Shipping Address\n\n Are you sure you want to delete this address?')" ></asp:Button>
              &nbsp;&nbsp; --%>
              <asp:Button ID="lnkbtnShipUpdate" Text="Update Address" CommandName="Update" runat="server" 
                ValidationGroup="grpEditShip" OnClientClick="javascript:warn_pageunload=true;" ></asp:Button>
            </EditItemTemplate>
          </asp:FormView>
        </asp:Panel>
      </td>
    </tr>
  </table>
    
  <br /><br /><asp:label ID="txtMsg" runat="server" Width="350" Visible="false" Height="20" ForeColor="Red" ></asp:label>
</asp:Content>
